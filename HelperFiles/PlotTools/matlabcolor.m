function rgb = matlabcolor(n)
%rgb = matlabcolor(n)
%   Returns n'th default matlab plot color (RGB triplets)
switch n
    case 1, rgb = [0, 0.4470, 0.7410];
    case 2, rgb = [0.8500, 0.3250, 0.0980];
    case 3, rgb = [0.9290, 0.6940, 0.1250];
    case 4, rgb = [0.4940, 0.1840, 0.5560];
    case 5, rgb = [0.4660, 0.6740, 0.1880];
    case 6, rgb = [0.3010, 0.7450, 0.9330];
    case 7, rgb = [0.6350, 0.0780, 0.1840];
end

