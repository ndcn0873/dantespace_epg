function complot_sub(x)
%   same as complot.m, but doesn't start a new figure (such that it's
%   useable for subfigures).
%   matthijs.debuck@ndcn.ox.ac.uk
    if isinteger(x)
        x = double(x);
    end
    
    if ~isreal(x)
        x = abs(x);
    end

    imagesc((squeeze(x))), colormap gray, %axis equal%, axis off

%     imagesc(flip(abs(squeeze(x)),1)), colormap gray, axis equal%, axis off
    if std(abs(x(:))) > 0
        caxis([min(abs(x(:))) prctile(abs(x(:)),99.9)])
    end
%     caxis([0 prctile(abs(x(:)),99.99)])
    hold on
end

