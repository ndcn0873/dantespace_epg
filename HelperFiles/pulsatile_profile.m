
function profile = pulsatile_profile(v,T,plotbool,timingfactor,type,no_newfigure)
%profile = pulsatile_profile(v,T,plotbool,timingfactor)
%
%   Approximate pulsatile flow profile for (~ICA) flow with mean velocity v
%   (cm/s) and total duration T (s). Assumes a heartrate of 60Hz such that
%   one cardiac cycle = 1 second. plotbool = flag on/off to plot result.
%   The profile starts at a random time in the pulsatile cycle, to correct
%   for the sequence (DANTE-SPACE) not using a gated acquisition (so it
%   averages out a range of random initial positions)
%   Alternatively, timingfactor can be set to choose the starting point of
%   the pulsatile cycle.
%   type selects whether a profile estimated based on 'CSF' or 'blood'
%   motion is used

if ~exist('T','var')
    T = 1;
end
if ~exist('plotbool','var')
    plotbool = false;
end
if ~exist('no_newfigure','var')
    no_newfigure = false;
end

repeats = ceil(T+1);

if isequal(type,'blood')
    % p-array based on ICA profile in Fig. 2 in https://journals.sagepub.com/doi/pdf/10.1243/095441103770802568
    p = [17 17.5 18 18.7 20 22 24 26.6 29 31 32.5 33.4 34.3 34.7 34.8 34.4 33.6 32.5 31.6 31 31.4 32.1 33.2 34.4 35.4 36.2 37 37.4 37.5 37 36.5 35.6 34.4 32.7 31 29.4 27.9 27 26.2 26.1 26.1 26.15 26.2 26 25.5 24.9 24.2 23.5 23.2 23 22.9 22.5 21.9 21.4 20.6 20.1 19.5 19.1 18.5 18.2 18 17.9 17.8 17.72 17.64 17.565 17.5 17.6 17.7 17.85 18 18.1 18.15 18.15 18.2 18.2 18.15 18.1 18 18 17.95 17.86 17.75 17.7 17.6 17.4 17.3 17.15 17 16.9];
elseif isequal(type,'CSF')
    % p-array based on phase-contrast measurements Fig. 12a in https://ieeexplore.ieee.org/document/1408112
    p = [5.7 5.6 5.2 4.8 2.7 1.6 1.0 0.2 -0.5 -0.7 -1.2 -2.1 -3.1 -3.8 -4.8 -5.4 -6.0 -6.0 -6.1 -5.9 -6.0 -5.9 -5.7 -4.9 -3.7 -1.8 0 1.1 3.0 3.4 3.3 4.5 5.7];
else 
    error('Invalid flow profile type')
end

%upsample to have smoother discretization in simulations
p = interp1(1:numel(p),p,1:0.2:numel(p));

profile = [];
for repeat = 1:repeats
    profile = [profile, p / mean(abs(p)) * v];
end

if ~exist('timingfactor','var') || isempty(timingfactor)
    init_time = rand(1);
else
    init_time = timingfactor;
    init_time = mod(init_time,1);
end
init_time = init_time + eps; %can't be zero --> eps = lazy work-around
profile = profile(ceil(init_time*numel(p)):ceil((init_time+T)*numel(p)));

if plotbool
    if ~no_newfigure, figure, end
    hold on, box on
    plot((1:numel(profile))/numel(p),profile,'r','LineWidth',2)
    if min(profile) < 0, plot([0 T], [0 0], '--k'), end
    title('Pulsatile velocity profile for <v> = '+string(v)'+' cm/s')
    xlabel('Time (s)')
    ylabel('Velocity (cm/s)')
    set(gca,'FontSize',15)
    xlim([0 T]), 
    if v~=0
        if min(profile) < 0, ylim([-1.2 1.2]*max(abs(profile)));
        else, ylim([0 max(profile)*1.2]);
        end
    end
end
end

