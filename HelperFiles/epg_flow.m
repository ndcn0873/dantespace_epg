function FpFmZ = epg_flow(FpFmZ,v,T,Ag,beta,n_grads_factor)
%FpFmZ = epg_flow(FpFmZ,v,T,Ag,beta)
% Flow operator for use in EPG formalism. Written as eq. 54 in Weigel2015,
% using code by B. Hargreaves (epg_grelax.m -> diffusion section).
% NOTE: somewhat unusual use of units because of initial implementation -->
%  --> not consistent with other functions
% 
% Parameters:
% v = flow velocity (cm/s)
% T = duration of gradient (ms)
% Ag = area below unit gradient (= \int_t G(t)dt ), (mT*ms/m)
% beta = angle between vec(v) and vec(k) i.e. angle between flow direction
%           and gradient, degrees
% n_grads_factor = number of "unit gradients" covered by the flow (see code
%           for more detail - defaults to 1)
%
% Original code by B. Hargreaves; modified for flow-correction by
% matthijs.debuck@ndcn.ox.ac.uk

if(~exist('beta','var')) 
    beta = 0; % Default: beta = 0 (i.e. flow direction = k/gradient direction)
end

if (~exist('n_grads_factor','var'))
    n_grads_factor = 1;
elseif ~(int8(n_grads_factor) == n_grads_factor)
    error('n_grads_factor needs to be an integer to make sense')
end

gamma = 42.577e6;       % H1-gyromagnetic ratio, Hz/T
Ag = Ag * 1e-6;         % mT*ms/m to T*s/m
T = T * 1e-3;           % ms to s
beta = beta * pi / 180; % degr to radians
v = v * 1e-2;           % cm/s to m/s

k = gamma * 2 * pi * Ag;

factor = n_grads_factor; 
            %In the current DANTE-SPACE implementation, each gradient consists of 2 "unit gradients"
            %to allow for sampling in the center. This factor corrects for
            %this factor of 2 difference in resulting k-index for each
            %state when calculating the velocity-based dephasing. See below
            %for a little script that I used to check the consistency.

Findex = 0:length(FpFmZ(1,:))-1;	% index of states, 0...N-1
exp_L = Findex/factor * k * v * T * cos(beta);      % J^L = exp(-1i * exp_L) (eq 53b)
exp_Tp = (Findex/factor + 0.5) * k * v * T * cos(beta); % J^T = exp(-1i * exp_T) (53a)
exp_Tm = (Findex/factor - 0.5) * k * v * T * cos(beta); % opposite index-value

FpFmZ(1,:) = FpFmZ(1,:) .* exp(-1i * exp_Tp);       % flow effects of F+ states
FpFmZ(2,:) = FpFmZ(2,:) .* exp(-1i * exp_Tm);       % flow effects of F- states
FpFmZ(3,:) = FpFmZ(3,:) .* exp(-1i * exp_L);        % flow effects of Z states
end

%% Script for correction factor:
% % Play around with different values for "factor" and "n_grads", and
% % compare the value of v at which the first unit phase cycle occurs
% % (--> it's only the same when n_grads == factor)

% vs = 0:1:65;
% n_grads = 2;    %number of gradients covered by epg_flow
% factor = 2;
% clear angles
% 
% for v = vs
%     Q = [0 0 1]';
%     Q = epg_rf(Q,pi/2,0);
%     for n = 1:n_grads, Q = epg_grad(Q); end
%     
%     Q = epg_flow(Q,v,1,40,0,factor);
%     angles(find(vs == v)) = angle(Q(1,1+n_grads));
% end
% 
% figure, 
% plot(vs,angles), xlabel('v (cm/s)'), ylabel('Phase'), hold,
% plot(vs,abs(angles-angles(1)))
% legend('Angle','Difference to v = 0')