function [protocol] = set_dantespace_parameters(type)
%[protocol] = set_dantespace_parameters(type)
%   Set initial DANTESPACE-params. This does nothing fancy, but makes it
%   easier to change the DANTE parameters for multiple scripts in one go.
%   If type == 'Li', DANTE-Params are set based on the Li2012 paper
%   If type == 'OV', the SPACE parameters are declared as well, based on
%       Viessmann's paper (2017) (OV = Olivia Viessmann)
%   If type == 'TdB', the SPACE parameters are declared as well, using the
%       ISMRM22 DANTE parameters (200*9 degree DANTE) (TdB = Thijs de Buck)
%   If type == 'TdBv2', updated optimum parameters (170 x 12 degrees)
%   If type == '3T', parameters for 3T are used
%
% matthijs.debuck@ndcn.ox.ac.uk

if ~exist('type','var') || strcmp(type,'Li')
    t_D = 2;            % ms
    alpha = 7;          % degrees, flip angle
    pw = 0.06;          % ms, pulse width
    gw = 0.05;          % ms, gap width
    Gz = 6;             % mT/m
    duration = 2000;    % ms
    Np_D = floor(duration/t_D);    % ms
elseif strcmp(type,'OV')
    t_D = 1.6;           % ms
    alpha = 10;          % degrees, flip angle
    pw = 0.15;           % ms, pulse width
    gw = 0.02;           % ms, gap width
    Gz = 40;             % mT/m
    Np_D = 300;          % number of pulses
    TR = 2.62;           % seconds
    
    % From Viessmann2017, Supp Info:
%     FAs_SPACE = [90.0   86.9   47.7   37.7   31.9   28.9   26.4   24.7   23.1   21.8   20.6   21.6   22.1   22.8   23.3   24.1   24.8   25.6   26.4   27.4   28.3   29.3   30.3   31.4   32.5   33.6   34.8   36.0   37.3   38.8   40.2   41.8   43.6   45.4   47.4   49.6   52.0   54.6   57.6   60.9   64.8   69.2   71.5   72.6   73.1   73.3   73.7   74.1   74.6   75.0   75.5   75.9   76.5   77.0   77.6   78.1   78.8   79.5   80.2   81.0   81.9   82.9   83.9   85.1   86.3   87.8   89.4   91.3   93.4   95.8   98.7  102.0  106.1  111.2  117.8  126.7 140.6];
%     t_S = 4.6;           % ms, pulse repeat time in SPACE block
    
    %Alternatively, from 7T DANTE-SPACE in IDEA: actually realized FAs
    %The ETL is slightly different from the value given in the OV paper,
    %resulting in a lower number of FAs with slightly different FAs
    FAs_SPACE = [90 87.6735  48.1527  38.042  32.2528  29.212  26.5914  24.8892  23.1661  21.8748  21.1106  22.0616  22.5691  23.2234  23.8383  24.6083  25.3696  26.2545  27.1393  28.1215  29.106  30.169  31.2293  32.3629  33.5051  34.7297  35.986  37.3441  38.762  40.3026  41.9287  43.6982  45.5832  47.6461  49.8777  52.3536  55.0871  58.1709  61.6496  65.6589  70.325  72.5418  73.6302  74.0324  74.2447  74.6519  75.1161  75.5985  76.0025  76.4682  76.9569  77.5161  78.0513  78.6417  79.2502  79.9458  80.6627  81.4571  82.2918  83.2394  84.2581  85.4033  86.6465  88.0649  89.6434  91.4544  93.4999  95.8893  98.6678  101.983  105.968  110.929  117.275  125.899  139.083];
    t_S = 4.64; %SPACE interpulse time (ms)
    
elseif strcmp(type,'TdB')
    t_D = 1.6;           % ms
    alpha = 9;          % degrees, flip angle
    pw = 0.15;           % ms, pulse width
    gw = 0.02;           % ms, gap width
    Gz = 40;             % mT/m
    Np_D = 200;          % number of pulses
    TR = 2.62;           % seconds
    
    %From 7T DANTE-SPACE in IDEA: actually realized FAs
    %The ETL is slightly different from the value given in the OV paper,
    %resulting in a lower number of FAs with slightly different FAs
    FAs_SPACE = [90 87.6735  48.1527  38.042  32.2528  29.212  26.5914  24.8892  23.1661  21.8748  21.1106  22.0616  22.5691  23.2234  23.8383  24.6083  25.3696  26.2545  27.1393  28.1215  29.106  30.169  31.2293  32.3629  33.5051  34.7297  35.986  37.3441  38.762  40.3026  41.9287  43.6982  45.5832  47.6461  49.8777  52.3536  55.0871  58.1709  61.6496  65.6589  70.325  72.5418  73.6302  74.0324  74.2447  74.6519  75.1161  75.5985  76.0025  76.4682  76.9569  77.5161  78.0513  78.6417  79.2502  79.9458  80.6627  81.4571  82.2918  83.2394  84.2581  85.4033  86.6465  88.0649  89.6434  91.4544  93.4999  95.8893  98.6678  101.983  105.968  110.929  117.275  125.899  139.083];
    t_S = 4.64;
    
elseif strcmp(type,'TdBv2')
    t_D = 1.4;           % ms
    alpha = 12;          % degrees, flip angle
    pw = 0.15;           % ms, pulse width
    gw = 0.02;           % ms, gap width
    Gz = 40;             % mT/m
    Np_D = 170;          % number of pulses
    TR = 2.62;           % seconds
    
    %From 7T DANTE-SPACE in IDEA: actually realized FAs
    %The ETL is slightly different from the value given in the OV paper,
    %resulting in a lower number of FAs with slightly different FAs
    FAs_SPACE = [90 87.6735  48.1527  38.042  32.2528  29.212  26.5914  24.8892  23.1661  21.8748  21.1106  22.0616  22.5691  23.2234  23.8383  24.6083  25.3696  26.2545  27.1393  28.1215  29.106  30.169  31.2293  32.3629  33.5051  34.7297  35.986  37.3441  38.762  40.3026  41.9287  43.6982  45.5832  47.6461  49.8777  52.3536  55.0871  58.1709  61.6496  65.6589  70.325  72.5418  73.6302  74.0324  74.2447  74.6519  75.1161  75.5985  76.0025  76.4682  76.9569  77.5161  78.0513  78.6417  79.2502  79.9458  80.6627  81.4571  82.2918  83.2394  84.2581  85.4033  86.6465  88.0649  89.6434  91.4544  93.4999  95.8893  98.6678  101.983  105.968  110.929  117.275  125.899  139.083];
    t_S = 4.64;

elseif strcmp(type,'3T')
    %Use values for 3T from IDEA
    FAs_SPACE = [90.0   100.504  58.1922  46.4456  40.0588  36.5435  33.5838  31.4718  29.3042  27.5126  26.3389  27.187  27.5743  28.1748  28.78  29.6201  30.4962  31.5648  32.6591  33.8901  35.1186  36.4338  37.7184  39.0791  40.4361  41.9107  43.4442  45.1467  46.9474  48.9242  50.997  53.2471  55.6349  58.2847  61.1952  64.4896  68.1553  72.3266  77.086  82.7256  89.5186  93.0098  94.1449  94.2201  94.3846  94.8685  95.1669  95.4716  95.8229  96.2821  96.6204  97.0593  97.5163  98.0416  98.4977  99.0963  99.694  100.36  101.021  101.846  102.664  103.596  104.593  105.77  106.98  108.404  109.975  111.785  113.779  116.17  118.874  122.106  125.968  130.764  136.857];
    
    %initial guesses (from 7T prot):
    pw = 0.15;           % ms, pulse width
    gw = 0.02;           % ms, gap width
    
    %from simulation-based optimizations - adjusted 2.12 --> 2.10 for TA
    TR = 2.1;           % seconds
    
    %VE11 3T IDEA matched protocol (V1)
    FAs_SPACE = [90	100.956632	58.466574	46.621288	40.147193	36.55972	33.524958	31.344275	29.111698	27.263535	26.732175	27.499497	27.923851	28.582727	29.251794	30.159937	31.104241	32.240237	33.401135	34.698513	35.987036	37.365161	38.715615	40.154818	41.604434	43.193218	44.858842	46.712406	48.675939	50.830626	53.098873	55.580033	58.249387	61.250239	64.586182	68.391412	72.677943	77.649795	83.496072	90.437814	93.867712	94.957829	94.975867	95.19297	95.682997	96.01339	96.305333	96.711476	97.175698	97.560359	98.004139	98.529442	99.070556	99.594909	100.223849	100.912297	101.621575	102.38889	103.284836	104.239301	105.269944	106.446245	107.774341	109.218335	110.869275	112.774555	114.923947	117.414251	120.370478	123.861631	128.109634	133.480594	140.556053];
    t_S = 4.76;          % ms
    Gz = 28;             % mT/m
    t_D = 1.4;           % ms
    Np_D = 250;          % number of pulses
    alpha = 12;          % degrees, flip angle
elseif strcmp(type,'7T_T1w')
    %Use values for single T1w 7T simulation from IDEA (as a first guess)
    FAs_SPACE = [90, 55.7489, 25.7742, 17.9534, 16.4362, 15.4657, 14.7067, 14.3743, 14.0735, 14.035, 13.9903, 14.1296, 14.2567, 14.5236, 14.7772, 15.143, 15.4972, 15.9424, 16.3742, 16.8784, 17.3653, 17.9063, 18.4227, 18.9757, 19.4969, 20.0399, 20.5464, 21.0667, 21.553, 22.054, 22.5313, 23.0324, 23.5257, 24.0552, 24.5918, 25.1732, 25.7695, 26.4099, 27.0624, 27.7497, 28.4404, 29.1551, 29.8685, 30.6045, 31.3469, 32.1241, 32.9261, 33.7804, 34.6758, 35.6319, 36.6316, 37.6872, 38.7816, 39.9301, 41.1265, 42.3971, 43.747, 45.1423, 46.0602, 46.9468, 47.7605, 48.547, 49.3408, 50.1782, 51.0761, 52.0427, 53.0686, 54.148, 55.2763, 56.4676, 57.7404, 59.1192, 60.6144, 62.233, 63.9791, 65.8767, 67.963, 70.2865, 72.8866, 75.8084, 79.12, 82.9416, 87.4357, 92.8199, 99.4349, 107.9263, 119.6411, 139.0075];
        
    TR = 1.14;           % seconds
    t_S = 4.05;          % ms
    
    t_D = 1.4;           % ms
    alpha = 12;          % degrees, flip angle
    pw = 0.15;           % ms, pulse width
    gw = 0.02;           % ms, gap width
    Gz = 40;             % mT/m
    Np_D = 100;          % number of pulses
end

n_TRs = 1;

% Load values into struct format (added after v2 update)
if exist('TR','var'),        protocol.TR        = TR;        end
if exist('n_TRs','var'),     protocol.n_TRs     = n_TRs;     end
if exist('Np_D','var'),      protocol.Np_D      = Np_D;      end
if exist('alpha','var'),     protocol.FA_DANTE  = alpha;     end
if exist('Gz','var'),        protocol.Gz        = Gz;        end
if exist('t_D','var'),       protocol.t_D       = t_D;       end
if exist('FAs_SPACE','var'), protocol.FAs_SPACE = FAs_SPACE; end
if exist('t_S','var'),       protocol.t_S       = t_S;       end
if exist('gw','var'),        protocol.gw        = gw;        end
if exist('pw','var'),        protocol.pw        = pw;        end

end

