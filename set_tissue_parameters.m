function [flow, T1, T2] = set_tissue_parameters(type, v_change_pct, n_vvals)
%[v, beta, T1, T2] = set_tissue_parameters(type, v_change_pct)
%
%   Set initial tissue-parameters. This does nothing fancy, but makes it
%   easier to change the DANTE parameters for multiple scripts in one go.
%   type = tissue type: 'VW', 'VWmoving', 'CSF', or 'blood' give 7T values;
%               'VW3T', 'VWmoving3T', 'CSF3T', or 'blood3T' give 3T values
%   v_change_pct = variation in velocity values in voxel (%; default = 10)
%   n_vvals = number of velocity values for dephasing (default = 100,
%                max 10000, best to keep >40)
%

if ~exist('type','var')
    type = 'VW';
end
if ~exist('v_change_pct','var')
    v_change_pct = 10;
end
if ~exist('n_vvals','var')
    n_vvals = 60;
end
if (v_change_pct > 100) || (v_change_pct < 0)
    error('Invalid value for v_change_pct')
end

%Set flow & diffusion properties
if strcmp(type,'VW') || strcmp(type,'VW3T')
    v = 0.;             % cm/s
    beta = 0;           % degrees, angle between gradient and motion (irrelevant in v=0 case)
elseif strcmp(type,'VWmoving') || strcmp(type,'VWmoving3T')
    v = 0.054;          % cm/s - Based on scan data - will write more info later
    beta = 0;           % degrees, angle between gradient and motion
    flow.profile = 'CSF';
elseif strcmp(type,'blood') || strcmp(type,'blood3T')
    v = 24;             %Augst et al 2003 (= pulsatile flow profile ref.); cm/s
    beta = 0;           % degrees, angle between gradient and motion (if not trajectory is used)
    beta_SPACE = 75;    
    flow.profile = 'blood';
    flow.pulsatile_bool = true;
    flow.diffusion = true;
elseif strcmp(type,'CSF') || strcmp(type,'CSF3T')
    beta = 57.3;        % degrees, angle between gradient and motion - average value on unit sphere
    beta_SPACE = beta;  
    v = 0.367;          % cm/s
    flow.profile = 'CSF';
    flow.pulsatile_bool = true;
    flow.diffusion = true;
    %https://insightsimaging.springeropen.com/articles/10.1186/s13244-019-0686-x/tables/3)
    %gives cm/s values for the cerebral aqueduct (0.8 cm/s), which could be 
    %considered an upper limit here. https://ieeexplore.ieee.org/document/1408112
    %(which is used for the profile) indicates 0.367 cm/s for the 3rd
    %ventricle (average absolute value of the MRI data in Fig 12a).
else
    error('Tissue type not recognised (use either VW, CSF, or blood)')
end

% T1 & T2 (depends on field strength - currently 3T / 7T, 7T = default)
if strcmp(type,'VW') || strcmp(type,'VWmoving')
    T1 = 1628;          % DOI: 10.1002/jmri.24601 // consistent with Viessmann2017
    T2 = 46;            % DOI: 10.1002/jmri.24601 // consistent with Viessmann2017
elseif strcmp(type,'VW3T') || strcmp(type,'VWmoving3T')
    T1 = 1227;          % DOI: 10.1002/jmri.24601
    T2 = 55;            % DOI: 10.1002/jmri.24601
elseif strcmp(type,'blood')
    T1 = 2290;          % as used by Viessmann2017, from Rane, Gore et al
    T2 = 100;           % as used by Viessmann2017
elseif strcmp(type,'blood3T')
    T1 = 1779;          % DOI: 10.2463/mrms.11.265
    T2 = 122;           % DOI: 10.1002/mrm.21342, for Hct = 0.44, Y = 0.99
elseif strcmp(type,'CSF')
    T1 = 4019;          % as used by Viessmann2017
    T2 = 311;           % as used by Viessmann2017
elseif strcmp(type,'CSF3T')
    T1 = 4019;          % "did not change with B0" (DOI: 10.1002/mrm.21122 and 10.1002/mrm.1910030214), so use the same value for both sims
    T2 = 517;           % No 7T literature values found; used an interpolation for 3T from the values B0 (T) = [0.05 0.15 0.6 1.5 7] / T2 (ms) = [1584 1760 2190 780 311]; refs: doi 10.1002/mrm.29009 & doi 10.1002/mrm.1910030214 & doi 10.1002/mrm.10661 & https://www.mr.ethz.ch/abstracts/files/ismrm13_2464.pdf; rough estimate, but seems minimally T2 dependent. Used m/sqrt(x) with x^2 error for fit, yielding m = 895
end

if v_change_pct > 0
    % Don't assume a single unique value, but average simulations for a range
    % of velocities centered around the mean velocity value; this also adds the
    % effect of dephasing spins, although it's hard to get accurate values for
    % the range/distribution to use. Not very sensitive though
    
    rng('default')
    sweep = normrnd(100,v_change_pct,10000,1)/100; sweep = sort(abs(sweep));
    sweep = sweep(ceil(end/2/n_vvals:end/n_vvals:end));     %downsample to desired nr of values
    sweep = sweep / mean(sweep);
else, sweep = 1;
end

if v > 0
    v = v*sweep;
end

% Add to struct
flow.vs     = v;
flow.beta   = beta;

if strcmp(type,'blood') || strcmp(type,'CSF') || strcmp(type,'blood3T') || strcmp(type,'CSF3T')
    flow.diffusion = true;
else
    flow.diffusion = false;
end

if exist('beta_SPACE','var')
    flow.beta_SPACE = beta_SPACE;
end

end

