For questions, please contact matthijs.debuck@ndcn.ox.ac.uk or thijsdebuck@live.nl.

The main file is epg_dantespace.m; three examples on how to use/run/visualize the simulations can be found in example.m. Those examples show:
1. Simple simulations to compare vessel wall and blood signal evolution
2. Simulations using B1 variations and flow trajectories to compare the achieved blood signal using ideal and realistic (7T CP) B1+
3. Simulation of the effect of vessel wall motion on the vessel wall signal, using different DANTE parameters

The folder 'Thesis' contains a single script which can be used to reproduce all figures in the simulations chapter of the thesis (see below). 

Some of the code in this repo requires the MATLAB Image Processing, Statistics and Machine Learning, Curve Fitting, and Parallel Computing toolboxes. 

This code was used for: 
- MHS de Buck, "Imaging cerebrovascular health using 7T MRI", DPhil thesis (2023).
- MHS de Buck, JL Kent, AT Hess, P Jezzard, “Parallel Transmit DANTE-SPACE for improved black-blood signal suppression at 7 Tesla”, 2022 ISMRM Annual Meeting, London, UK.
- MHS de Buck, JL Kent, AT Hess, P Jezzard, “Improved vessel wall imaging at 7T using DANTE-SPACE with neck-specific pTx shims”, 2022 ISMRM High Field Workshop, Lisbon, Portugal.
- MHS de Buck, AT Hess, P Jezzard, “Optimized DANTE preparation for intracranial DANTE-SPACE vessel wall imaging at 7T”, 2021 ISMRM Annual Meeting (virtual).
