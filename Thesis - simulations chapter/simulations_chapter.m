clc, clear
time_start = datetime();

%% PART 1 - validation
%% 1.1) DANTE: EPG vs Bloch ensemble validation
% Relative to Li et al.: DOI 10.1002/mrm.24142, Figure 3a

% run other script to simulate/plot; takes ~ 1.5 min with CPU
DANTE_Bloch_ensemble_vs_EPG

%% 1.2) SPACE: prescribed vs simulated signal evolution
disp('Starting 1.2')
% Set simulation variables
prot = set_dantespace_parameters('OV');
[flow, T1, T2] = set_tissue_parameters('VW');

opts.no_newfigure = true; opts.no_legend = false;
figure('Position',[100 200 1600 500])

% Show theoretical SPACE-profile
t = 0.1:0.1:100;  %evolution is prescribed based on 100ms
S = zeros(size(t));
S(1:119) = exp(-t(1:119)/17.5);
S(120:534) = S(t == 11.9);
S(535:end) = exp(-t(1:466)/29) * S(119);

% Scale to total SPACE duration
t = t / 1e5 * prot.t_S * (numel(prot.FAs_SPACE) + 1);
t_SPACE = (1:numel(prot.FAs_SPACE)) * prot.t_S * 1e-3; 

subplot(1,3,1), hold on, box on
plot([0, t],[0, S],'k','LineWidth',3)
xlim([0 inf]), ylim([0 1])
xlabel('Time (s)'), ylabel('Normalized signal')
yyaxis right
plot(t_SPACE,prot.FAs_SPACE,'o','LineWidth',2,'MarkerSize',5)
ylabel('FA (�)','rotation',270)
title('a) Prescribed signal evolution')
ylim([0 180]), yticks(0:60:180)
legend('M_{XY}','FA')
set(gca,'FontSize',15)

% Show basic simulations with & without DANTE (vessel wall)
subplot(1,3,2)
prot.Np_D = 0;
epg_dantespace(prot, T1, T2, 1, flow, opts);
title('b) EPG - Without DANTE')
ylim([0 0.43]), xlim([0 0.37])
yticks(0:0.1:0.4)

subplot(1,3,3)
prot.Np_D = 300;
epg_dantespace(prot, T1, T2, 1, flow, opts);
title('c) EPG - With DANTE')
ylim([0 0.43]), xlim([0.45 0.85])
yticks(0:0.1:0.4)

%% 1.3) 3 tissue types 
disp('Starting 1.3')
% Set simulation variables
prot = set_dantespace_parameters('OV');

opts.no_newfigure = true;
figure('Position',[100 200 1600 500])

subplot(1,3,1), opts.no_legend = true;
[flow, T1, T2] = set_tissue_parameters('VW');
prot.n_TRs = 2;
epg_dantespace(prot, T1, T2, 1, flow, opts);
title('a) Vessel Wall (2nd TR)')
ylim([0 0.3]), xlim(prot.TR + [-0.05 0.87])

subplot(1,3,2), opts.no_legend = false;
[flow, T1, T2] = set_tissue_parameters('CSF'); flow.vs = mean(flow.vs); %no v-averaging
flow.pulsatile_bool = false; flow.diffusion = false;
prot.n_TRs = 2;
epg_dantespace(prot, T1, T2, 1, flow, opts);
title('b) CSF (2nd TR)')
ylim([0 0.05]), xlim(prot.TR + [-0.05 0.87])
ylabel('')

subplot(1,3,3), opts.no_legend = true;
[flow, T1, T2] = set_tissue_parameters('blood'); flow.vs = mean(flow.vs); %no v-averaging
flow.pulsatile_bool = false;
flow.diffusion = false;
prot.n_TRs = 1;
epg_dantespace(prot, T1, T2, 1, flow, opts);
title('c) Blood (1st TR)')
ylim([0 0.05])
ylabel('')

%% PART 2 - additional variations to simulations
%% 2.1) Velocity variation (intravoxel)
disp('Starting 2.1')
% Set simulation variables
prot = set_dantespace_parameters('OV'); 
[flow, T1, T2] = set_tissue_parameters('CSF');
prot.n_TRs = 2; flow.diffusion = false;
opts.no_newfigure = true;

figure('Position',[100 100 1600 800])

% CSF results
subplot(2,3,1)
histogram(flow.vs,0:mean(flow.vs)/15:2*mean(flow.vs))
xlim([0 mean(flow.vs)*2])
ylabel('Nr. of simulations')
title('a) CSF - Velocity distribution')
set(gca,'FontSize',15)

subplot(2,3,3)  % Run this first, but plot it to the right
flow.pulsatile_bool = false; opts.no_legend = true;
epg_dantespace(prot, T1, T2, 1, flow, opts);
ylim([0 0.05]), xlim(prot.TR + [-0.05 0.87])
title('c) CSF - With intravoxel averaging')
xlabel('')

subplot(2,3,2)
flow.pulsatile_bool = false; opts.no_legend = false;
flow.vs = mean(flow.vs);
epg_dantespace(prot, T1, T2, 1, flow, opts);
ylim([0 0.05]), xlim(prot.TR + [-0.05 0.87])
title('b) CSF - Single velocity')
xlabel('')

% Blood results
[flow, T1, T2] = set_tissue_parameters('blood');
prot.n_TRs = 1; flow.diffusion = false;

subplot(2,3,4)
histogram(flow.vs,0:mean(flow.vs)/15:2*mean(flow.vs))
xlim([0 mean(flow.vs)*2])
xlabel('|<v>| (cm/s)'), ylabel('Nr. of simulations')
title('d) Blood - Velocity distribution')
set(gca,'FontSize',15)

subplot(2,3,6)  % Run this first, but plot it to the right
flow.pulsatile_bool = false; opts.no_legend = true;
epg_dantespace(prot, T1, T2, 1, flow, opts);
ylim([0 0.05]), xlim([0 0.87])
title('f) Blood - With intravoxel averaging')

subplot(2,3,5)
flow.pulsatile_bool = false; opts.no_legend = true;
flow.vs = mean(flow.vs);
epg_dantespace(prot, T1, T2, 1, flow, opts);
ylim([0 0.05]), xlim([0 0.87])
title('e) Blood - Single velocity')

%% 2.2) Pulsatile flow 
disp('Starting 2.2')
% Set simulation variables
prot = set_dantespace_parameters('OV'); 
[flow, T1, T2] = set_tissue_parameters('CSF');
prot.n_TRs = 2; flow.diffusion = false;
opts.no_newfigure = true;

figure('Position',[100 100 1600 800])
subplot(2,3,1)
pulsatile_profile(mean(flow.vs),4,1,0,'CSF',1);
xlabel('')
title('a) CSF - Flow profile')
set(gca,'FontSize',15)

subplot(2,3,2)
flow.pulsatile_bool = false; opts.no_legend = false;
epg_dantespace(prot, T1, T2, 1, flow, opts);
ylim([0 0.05]), xlim(prot.TR + [-0.05 0.87])
title('b) CSF - Without pulsatility')
xlabel('')

subplot(2,3,3)
flow.pulsatile_bool = true; opts.no_legend = true;
epg_dantespace(prot, T1, T2, 1, flow, opts);
ylim([0 0.05]), xlim(prot.TR + [-0.05 0.87])
title('c) CSF - With pulsatility')
xlabel('')

% Bottom row - blood simulations
[flow, T1, T2] = set_tissue_parameters('blood'); prot.n_TRs = 1;

subplot(2,3,4)
pulsatile_profile(mean(flow.vs),4,1,0,'blood',1);
title('d) Blood - Flow profile')
set(gca,'FontSize',15)

subplot(2,3,5)
flow.pulsatile_bool = false; flow.diffusion = false;
prot.n_TRs = 1;
epg_dantespace(prot, T1, T2, 1, flow, opts);
ylim([0 0.05]), xlim([0 0.87])
title('e) Blood - Without pulsatility')

subplot(2,3,6)
flow.pulsatile_bool = true;
epg_dantespace(prot, T1, T2, 1, flow, opts);
ylim([0 0.05]), xlim([0 0.87])
title('f) Blood - With pulsatility')

%% 2.3) Flow trajectories
disp('Starting 2.3')
% Load trajectory & structural reference
load('HelperFiles/CH3_example_data.mat', 'x', 'y', 'MPRAGE_coronalslab')
MPRAGE_coronalslice = mip(MPRAGE_coronalslab,3);

% Set simulation variables
prot = set_dantespace_parameters('OV'); 
[flow, T1, T2] = set_tissue_parameters('blood');
flow.diffusion = false; flow.pulsatile_bool = false;
opts.no_newfigure = true;

figure('Position',[100 200 1600 500])
subplot(1,3,1)
complot_sub(MPRAGE_coronalslice), hold on, axis off
plot(x,y,'r', 'LineWidth',2.5)
title('a) 2D vessel trajectory')
set(gca, 'FontSize', 15)

subplot(1,3,2)
opts.no_legend = false;
epg_dantespace(prot, T1, T2, 1, flow, opts);
ylim([0 0.05]), xlim([0 0.87])
title('b) Blood - Constant flow direction')

subplot(1,3,3)
opts.no_legend = true;
flow.location_coords = [x; y]; flow = rmfield(flow,'beta');
epg_dantespace(prot, T1, T2, 1, flow, opts);
ylim([0 0.05]), xlim([0 0.87])
title('c) Blood - Using flow trajectory')

%% 2.4) Diffusion
disp('Starting 2.4')
% Set simulation variables
prot = set_dantespace_parameters('OV'); 

figure('Position',[350 100 1100 800])
opts.no_newfigure = true;

% Top row: CSF
[flow, T1, T2] = set_tissue_parameters('CSF');
flow.pulsatile_bool = false; 
prot.n_TRs = 2; flow.diffusion = false;

subplot(2,2,1), opts.no_legend = true;
epg_dantespace(prot, T1, T2, 1, flow, opts);
ylim([0 0.05]), xlim(prot.TR + [-0.05 0.87])
title('a) CSF - Without diffusion')
xlabel(' ') 

subplot(2,2,2), opts.no_legend = false;
flow.diffusion = true;
epg_dantespace(prot, T1, T2, 1, flow, opts);
ylim([0 0.05]), xlim(prot.TR + [-0.05 0.87])
title('b) CSF - With diffusion')
xlabel(' '), ylabel(' ')

% Bottom row: blood
[flow, T1, T2] = set_tissue_parameters('blood');
flow.pulsatile_bool = false;
prot.n_TRs = 1; flow.diffusion = false;

subplot(2,2,3), opts.no_legend = true;
epg_dantespace(prot, T1, T2, 1, flow, opts);
ylim([0 0.05]), xlim([0 0.87])
title('c) Blood - Without diffusion')

subplot(2,2,4)
flow.diffusion = true;
epg_dantespace(prot, T1, T2, 1, flow, opts);
ylim([0 0.05]), xlim([0 0.87])
title('d) Blood - With diffusion')
ylabel(' ') 

%% 2.5) Blood B1+ variations
disp('Starting 2.6')
% Load trajectory & B1 data
load('HelperFiles/CH3_example_data.mat', 'x', 'y', 'B1_CP_slice')
x = floor(x / 192 * 40 + 1); y = ceil(y / 256 * 40 + 1); %adjust from MPRAGE resolution to B1 resolution

% Extract B1-profiles along "vessel" (magnitude only for 1Tx)
B1_CP_vessel = improfile(B1_CP_slice,x,y);
B1_CP_vessel = smooth(B1_CP_vessel,3);

% Set simulation variables
prot = set_dantespace_parameters('OV'); 
[flow, T1, T2] = set_tissue_parameters('blood');
flow.diffusion = false; flow.pulsatile_bool = false;
opts.no_newfigure = true;

figure('Position',[100 100 900 800])

subplot(2,2,1)
complot_sub(B1_CP_slice), hold on, axis off
plot(x,y,'r', 'LineWidth',2.5)
title('a) B1^+ map & trajectory')
set(gca, 'FontSize', 15)
caxis([0 1]), colorbar

subplot(2,2,2), hold on, box on
cm_locs = (0:numel(B1_CP_vessel)-1)/numel(B1_CP_vessel)*25;
plot(cm_locs, B1_CP_vessel,'LineWidth',2)
xlim([0 max(cm_locs)]), ylim([0 1])
yticks([0 0.5 1]), yticklabels({'0','.5','1'})
xlabel('Distance (cm)'), ylabel('Relative B1^+')
title('b) B1^+ along trajectory')
set(gca, 'FontSize', 15)

subplot(2,2,3)
opts.no_legend = true;
epg_dantespace(prot, T1, T2, 1, flow, opts);
flow.B1 = 1;
ylim([0 0.05]), xlim([0 0.87])
title('c) Blood - Relative B1^+ = '+string(flow.B1))

subplot(2,2,4)
opts.no_legend = true;
flow.B1 = B1_CP_vessel; 
epg_dantespace(prot, T1, T2, 1, flow, opts);
ylim([0 0.05]), xlim([0 0.87])
title('d) Blood - B1^+ along trajectory')
ylabel(' '), yticklabels([])

%% 2.6) All changes combined
disp('Starting 2.7')
% Set simulation variables
prot = set_dantespace_parameters('OV');

opts.no_newfigure = true;
figure('Position',[350 100 1100 800])

% Top row: CSF
subplot(2,2,1), opts.no_legend = true;
[flow, T1, T2] = set_tissue_parameters('CSF'); flow.vs = mean(flow.vs);
flow.pulsatile_bool = false;
flow.diffusion = false;
prot.n_TRs = 2;
epg_dantespace(prot, T1, T2, 1, flow, opts);
title('a) CSF - Basic simulations')
ylim([0 .05]), xlim(prot.TR + [-0.05 0.87])
xlabel('')

subplot(2,2,2), opts.no_legend = false;
[flow, T1, T2] = set_tissue_parameters('CSF');
flow.pulsatile_bool = true;
flow.diffusion = true;
epg_dantespace(prot, T1, T2, 1, flow, opts);
title('b) CSF - Complete simulations')
ylim([0 .05]), xlim(prot.TR + [-0.05 0.87])
ylabel(''), xlabel('')

% Bottom row: Blood
subplot(2,2,3), opts.no_legend = true;
[flow, T1, T2] = set_tissue_parameters('blood'); flow.vs = mean(flow.vs);
flow.pulsatile_bool = false;
flow.diffusion = false;
prot.n_TRs = 1;
epg_dantespace(prot, T1, T2, 1, flow, opts);
title('c) Blood - Basic simulations')
ylim([0 .05])

subplot(2,2,4), opts.no_legend = true;
[flow, T1, T2] = set_tissue_parameters('blood');
flow.pulsatile_bool = true;
flow.diffusion = true;
flow.B1 = B1_CP_vessel; 
flow.location_coords = [x; y]; flow = rmfield(flow,'beta');
epg_dantespace(prot, T1, T2, 1, flow, opts);
title('d) Blood - Complete simulations')
ylim([0 .05])
ylabel('')

%% PART 3 - Effect of relaxation & physiological parameters
%% 3.1) T1 & T2
disp('Starting 3.1')
rel_values = 0.7:0.025:1.3;
% rel_values = 0.7:0.025:3.3;   %to test effect of vastly different CSF T2

% Set simulation variables
prot_VW_CSF = set_dantespace_parameters('OV'); 
prot_VW_CSF.n_TRs = 2;
prot_blood = set_dantespace_parameters('OV'); 
prot_blood.n_TRs = 1;

% Initialize output parameters
VW_sig_T1 = zeros(size(rel_values));
CSF_sig_T1 = zeros(size(rel_values));
blood_sig_T1 = zeros(size(rel_values));
VW_sig_T2 = zeros(size(rel_values));
CSF_sig_T2 = zeros(size(rel_values));
blood_sig_T2 = zeros(size(rel_values));

% Set tissue/flow parameters (Note: Pulsatility/Diffusion are turned on by
% default for CSF/blood)
[flow_VW, T1_VW, T2_VW] = set_tissue_parameters('VW');
[flow_CSF, T1_CSF, T2_CSF] = set_tissue_parameters('CSF');
[flow_blood, T1_blood, T2_blood] = set_tissue_parameters('blood');
flow_blood.B1 = B1_CP_vessel; 
flow_blood.location_coords = [x; y]; flow_blood = rmfield(flow_blood,'beta');

opts = [];

% Parameter sweep (takes a while, depending on numel(rel_values))
parfor i = 1:numel(rel_values)
    [~,~,~,~,~,VW_sig_T1(i)] = epg_dantespace(prot_VW_CSF, T1_VW*rel_values(i), T2_VW, 0, flow_VW);
    [~,~,~,~,~,VW_sig_T2(i)] = epg_dantespace(prot_VW_CSF, T1_VW, T2_VW*rel_values(i), 0, flow_VW);
    
    [~,~,~,~,~,CSF_sig_T1(i)] = epg_dantespace(prot_VW_CSF, T1_CSF*rel_values(i), T2_CSF, 0, flow_CSF);
    [~,~,~,~,~,CSF_sig_T2(i)] = epg_dantespace(prot_VW_CSF, T1_CSF, T2_CSF*rel_values(i), 0, flow_CSF);
    
    [~,~,~,~,~,blood_sig_T1(i)] = epg_dantespace(prot_blood, T1_blood*rel_values(i), T2_blood, 0, flow_blood);
    [~,~,~,~,~,blood_sig_T2(i)] = epg_dantespace(prot_blood, T1_blood, T2_blood*rel_values(i), 0, flow_blood);
end

%% Plot results
figure('Position',[100 100 1600 600])
rel_ylims = [0 2];
ref_sig_VW = VW_sig_T1(ceil(numel(rel_values)/2));
ref_sig_CSF = CSF_sig_T1(ceil(numel(rel_values)/2));
ref_sig_blood = blood_sig_T1(ceil(numel(rel_values)/2));

% VW plots
subplot(2,3,1), hold on, box on
set(gca,'FontSize',15)
plot(T1_VW/1000*rel_values,VW_sig_T1,'o-','LineWidth',2)
plot(T1_VW/1000*rel_values,ones(size(rel_values))*ref_sig_VW,'k--')
plot([T1_VW T1_VW]/1000, rel_ylims*ref_sig_VW,'k--')
xlabel('T1 (s)'), ylabel('Signal / M_0')
xlim([-inf inf]), ylim([-inf inf])
title('a) Vessel Wall - T1')

subplot(2,3,4), hold on, box on
set(gca,'FontSize',15)
plot(T2_VW/1000*rel_values,VW_sig_T2,'o-','LineWidth',2)
plot(T2_VW/1000*rel_values,ones(size(rel_values))*ref_sig_VW,'k--')
plot([T2_VW T2_VW]/1000, rel_ylims*ref_sig_VW,'k--')
xlabel('T2 (s)'), ylabel('Signal / M_0')
xlim([-inf inf]), ylim([-inf inf])
title('d) Vessel Wall - T2')

% CSF plots
subplot(2,3,2), hold on, box on
set(gca,'FontSize',15)
plot(T1_CSF/1000*rel_values,CSF_sig_T1,'o-','LineWidth',2,'color',matlabcolor(2))
plot(T1_CSF/1000*rel_values,ones(size(rel_values))*ref_sig_CSF,'k--')
plot([T1_CSF T1_CSF]/1000, rel_ylims*ref_sig_CSF,'k--')
xlabel('T1 (s)'),% ylabel('Signal (M0)')
xlim([-inf inf]), ylim([-inf inf])
title('b) CSF - T1')

subplot(2,3,5), hold on, box on
set(gca,'FontSize',15)
plot(T2_CSF/1000*rel_values,CSF_sig_T2,'o-','LineWidth',2,'color',matlabcolor(2))
plot(T2_CSF/1000*rel_values,ones(size(rel_values))*ref_sig_CSF,'k--')
plot([T2_CSF T2_CSF]/1000, rel_ylims*ref_sig_CSF,'k--')
xlabel('T2 (s)'),% ylabel('Signal (M0)')
xlim([-inf inf]), ylim([-inf inf])
title('e) CSF - T2')

% Blood plots
subplot(2,3,3), hold on, box on
set(gca,'FontSize',15)
plot(T1_blood/1000*rel_values,blood_sig_T1,'o-','LineWidth',2,'color',matlabcolor(3))
plot(T1_blood/1000*rel_values,ones(size(rel_values))*ref_sig_blood,'k--')
plot([T1_blood T1_blood]/1000, rel_ylims*ref_sig_blood,'k--')
xlabel('T1 (s)'),% ylabel('Signal (M0)')
xlim([-inf inf]), ylim([-inf inf])
title('c) Blood - T1')

subplot(2,3,6), hold on, box on
set(gca,'FontSize',15)
plot(T2_blood/1000*rel_values,blood_sig_T2,'o-','LineWidth',2,'color',matlabcolor(3))
plot(T2_blood/1000*rel_values,ones(size(rel_values))*ref_sig_blood,'k--')
plot([T2_blood T2_blood]/1000, rel_ylims*ref_sig_blood,'k--')
xlabel('T2 (s)'),% ylabel('Signal (M0)')
xlim([-inf inf]), ylim([-inf inf])
title('f) Blood - T2')

%% 3.2) Average flow velocity
disp('Starting 3.2')
vs_VW = [0:0.01:0.1, 0.11:0.02:0.25];
vs_CSF = [0:0.02:0.17, 0.2:0.05:0.5];
vs_blood = [0:0.5:3, 4:2:30];

% Set simulation variables
prot = set_dantespace_parameters('OV'); 

% Initialize output parameters
VW_sig_vs = zeros(size(vs_VW));
CSF_sig_vs = zeros(size(vs_CSF));
blood_sig_vs = zeros(size(vs_blood));

% Parameter sweep (each tissue type separately)
disp('Starting VW sweep')
prot.n_TRs = 2;
parfor i = 1:numel(vs_VW)
    [flow, T1, T2] = set_tissue_parameters('VWmoving');
    flow.vs = flow.vs / mean(abs(flow.vs)) * vs_VW(i);
    
    [~,~,~,~,~,VW_sig_vs(i)] = epg_dantespace(prot, T1, T2, 0, flow, opts);
end

disp('Starting CSF sweep')
prot.n_TRs = 2;
parfor i = 1:numel(vs_CSF)
    [flow, T1, T2] = set_tissue_parameters('CSF');
    flow.vs = flow.vs / mean(abs(flow.vs)) * vs_CSF(i);
    
    [~,~,~,~,~,CSF_sig_vs(i)] = epg_dantespace(prot, T1, T2, 0, flow, opts);
end

disp('Starting blood sweep')
prot.n_TRs = 1;
parfor i = 1:numel(vs_blood)
    [flow, T1, T2] = set_tissue_parameters('blood');
    flow.vs = flow.vs / mean(abs(flow.vs)) * vs_blood(i);
    flow.B1 = B1_CP_vessel; % flow.beta_SPACE = 90; %for test
    flow.location_coords = [x; y]; flow = rmfield(flow,'beta');
    
    [~,~,~,~,~,blood_sig_vs(i)] = epg_dantespace(prot, T1, T2, 0, flow, opts);
end

%% Plot
figure('Position',[100 200 1600 350])

subplot(1,3,1), hold on, box on
title('a) Vessel Wall')
set(gca,'FontSize',15)
plot(vs_VW,VW_sig_vs,'o-','LineWidth',2)
plot([0 0], [0 0.125], 'k--')
xlabel('v (cm/s)'), ylabel('Signal / M_0')
xlim([-inf inf]), ylim([0 0.1])

subplot(1,3,2), hold on, box on
title('b) CSF')
set(gca,'FontSize',15)
plot(vs_CSF,CSF_sig_vs,'o-','LineWidth',2,'color',matlabcolor(2))
plot([0.367 0.367], [0 0.125], 'k--')
xlabel('v (cm/s)'), %ylabel('Signal (M0)')
xlim([-inf inf]), ylim([0 0.1])

subplot(1,3,3), hold on, box on
title('c) Blood')
set(gca,'FontSize',15)
plot(vs_blood,blood_sig_vs,'o-','LineWidth',2,'color',matlabcolor(3))
plot([24 24], [0 0.125], 'k--')
xlabel('v (cm/s)'), %ylabel('Signal (M0)')
xlim([-inf inf]), ylim([0 0.1])

%% 3.3a) Intravoxel dephasing
disp('Starting 3.3a')
pct_values = [0:0.25:0.25,1:15];

% Set simulation variables
prot = set_dantespace_parameters('OV'); 

% Initialize output parameters
CSF_sig_dephpct = zeros(size(pct_values));
blood_sig_dephpct = zeros(size(pct_values));

opts = [];

% Parameter sweep - CSF
prot.n_TRs = 2;
parfor i = 1:numel(pct_values)
    [flow, T1, T2] = set_tissue_parameters('CSF',pct_values(i));
    [~,~,~,~,~,CSF_sig_dephpct(i)] = epg_dantespace(prot, T1, T2, 0, flow, opts);
end

% Parameter sweep - blood
prot.n_TRs = 1;
parfor i = 1:numel(pct_values)
    [flow, T1, T2] = set_tissue_parameters('blood',pct_values(i));
    flow.B1 = B1_CP_vessel; 
    flow.location_coords = [x; y]; flow = rmfield(flow,'beta');
    [~,~,~,~,~,blood_sig_dephpct(i)] = epg_dantespace(prot, T1, T2, 0, flow, opts);
end

%% 3.3b) CSF Flow orientations
disp('Starting 3.3b')
betas = [0:5:65, 68:2:112, 115:5:180];  %degrees

% Set simulation variables
prot = set_dantespace_parameters('OV'); 
prot.n_TRs = 2;

% Initialize output parameters
CSF_sig_betas = zeros(size(betas));

opts = [];

% Parameter sweep - CSF
parfor i = 1:numel(betas)
    [flow, T1, T2] = set_tissue_parameters('CSF');
    flow.beta = betas(i);
    flow.beta_SPACE = [];
    [~,~,~,~,~,CSF_sig_betas(i)] = epg_dantespace(prot, T1, T2, 0, flow, opts);
end

%% 3.3c) Vessel Wall diffusion
disp('Starting 3.3c')
diff_coeffs = (0:0.15:3) * 1e-9;

% Set simulation variables
prot = set_dantespace_parameters('OV'); 
prot.n_TRs = 2;

% Initialize output parameters
VW_sig_diffcoeffs = zeros(size(diff_coeffs));
VWmov_sig_diffcoeffs = zeros(size(diff_coeffs));

opts = [];

% Parameter sweep
parfor i = 1:numel(diff_coeffs)
    [flow, T1, T2] = set_tissue_parameters('VW');
    flow.diffusion = true; flow.diff_coeff = diff_coeffs(i);
    [~,~,~,~,~,VW_sig_diffcoeffs(i)] = epg_dantespace(prot, T1, T2, 0, flow, opts);
    
    [flow, T1, T2] = set_tissue_parameters('VWmoving');
    flow.diffusion = true; flow.diff_coeff = diff_coeffs(i);
    flow.vs = flow.vs / mean(flow.vs) * 0.050;
    [~,~,~,~,~,VWmov_sig_diffcoeffs(i)] = epg_dantespace(prot, T1, T2, 0, flow, opts);
end

% [flow_VWmov, ~, ~] = set_tissue_parameters('VWmoving');
% v_VWmov = mean(flow_VWmov.vs); clear flow_VWmov

%% Plot (3.3a/b/c)
figure('Position',[100 200 1600 500])

subplot(1,3,1), hold on, box on
set(gca,'FontSize',15)
plot(pct_values,CSF_sig_dephpct,'o-','LineWidth',2,'color',matlabcolor(2))
plot(pct_values,blood_sig_dephpct,'o-','LineWidth',2,'color',matlabcolor(3))
plot([10 10], [0 0.125], 'k--')
legend('CSF','Blood','Default','location','SouthWest')
xlabel('\sigma (%)'), ylabel('Signal / M_0')
xlim([-inf inf]), ylim([0 0.013])
title('a) Intravoxel velocity averaging')

subplot(1,3,2), hold on, box on
set(gca,'FontSize',15)
plot(betas,CSF_sig_betas,'o-','LineWidth',2,'color',matlabcolor(2))
plot([57.3 57.3], [0 0.1], 'k--')
legend('CSF','Default')
xlabel('Angle w.r.t. gradient (�)'), ylabel('CSF Signal / M_0')
xlim([-inf inf]), ylim([0 .1])
title('b) CSF pulsation direction')

subplot(1,3,3), hold on, box on
set(gca,'FontSize',15)
plot(diff_coeffs*1e9,VW_sig_diffcoeffs,'o-','Color',matlabcolor(1),'LineWidth',2)
plot(diff_coeffs*1e9,VWmov_sig_diffcoeffs,'o-','Color',matlabcolor(4),'LineWidth',2)
legend('Stationary',string(0.05)+' cm/s','Location','best')
xlabel('Diffusion coefficient (\mum^2/ms)'), ylabel('VW Signal / M_0')
xlim([-inf inf]), ylim([0 .1])
title('c) Vessel Wall diffusion')

%% PART 4 - Effect of changing sequence parameters
%% 4.1a) DANTE Flip Angle
disp('Starting 4.1a')
FAs = 0:20; %NOTE: FA < 1 will trigger a units-warning; degrees

% Initialize output parameters
VW_sig_FA = zeros(size(FAs));
CSF_sig_FA = zeros(size(FAs));
blood_sig_FA = zeros(size(FAs));

% Set tissue/flow parameters 
[flow_VW, T1_VW, T2_VW] = set_tissue_parameters('VW');
[flow_CSF, T1_CSF, T2_CSF] = set_tissue_parameters('CSF');
[flow_blood, T1_blood, T2_blood] = set_tissue_parameters('blood');
flow_blood.B1 = B1_CP_vessel; 
flow_blood.location_coords = [x; y]; flow_blood = rmfield(flow_blood,'beta');

% Parameter sweep 
parfor i = 1:numel(FAs)
    prot = set_dantespace_parameters('OV'); 
    prot.FA_DANTE = FAs(i);
    
    [~,~,~,~,~,blood_sig_FA(i)] = epg_dantespace(prot, T1_blood, T2_blood, 0, flow_blood); %1 TR
    
    prot.n_TRs = 2;
    [~,~,~,~,~,VW_sig_FA(i)] = epg_dantespace(prot, T1_VW, T2_VW, 0, flow_VW); %2 TRs
    [~,~,~,~,~,CSF_sig_FA(i)] = epg_dantespace(prot, T1_CSF, T2_CSF, 0, flow_CSF); %2 TRs
end

%% 4.1b) Nr of DANTE pulses
disp('Starting 4.1b')
Np_Ds = 0:20:400;

% Initialize output parameters
VW_sig_NpD = zeros(size(Np_Ds));
CSF_sig_NpD = zeros(size(Np_Ds));
blood_sig_NpD = zeros(size(Np_Ds));

% Set tissue/flow parameters
[flow_VW, T1_VW, T2_VW] = set_tissue_parameters('VW');
[flow_CSF, T1_CSF, T2_CSF] = set_tissue_parameters('CSF');
[flow_blood, T1_blood, T2_blood] = set_tissue_parameters('blood');
flow_blood.B1 = B1_CP_vessel; 
flow_blood.location_coords = [x; y]; flow_blood = rmfield(flow_blood,'beta');

% Parameter sweep 
parfor i = 1:numel(Np_Ds)
    prot = set_dantespace_parameters('OV'); 
    prot.Np_D = Np_Ds(i);
    
    [~,~,~,~,~,blood_sig_NpD(i)] = epg_dantespace(prot, T1_blood, T2_blood, 0, flow_blood); %1 TR
    
    prot.n_TRs = 2;
    [~,~,~,~,~,VW_sig_NpD(i)] = epg_dantespace(prot, T1_VW, T2_VW, 0, flow_VW); %2 TRs
    [~,~,~,~,~,CSF_sig_NpD(i)] = epg_dantespace(prot, T1_CSF, T2_CSF, 0, flow_CSF); %2 TRs
end

%% 4.1c) DANTE gradient strength
disp('Starting 4.1c')
Gz = 0:2:50;    % mT/m

% Initialize output parameters
VW_sig_Gz = zeros(size(Gz));
CSF_sig_Gz = zeros(size(Gz));
blood_sig_Gz = zeros(size(Gz));

% Set tissue/flow parameters 
[flow_VW, T1_VW, T2_VW] = set_tissue_parameters('VW');
[flow_CSF, T1_CSF, T2_CSF] = set_tissue_parameters('CSF');
[flow_blood, T1_blood, T2_blood] = set_tissue_parameters('blood');
flow_blood.B1 = B1_CP_vessel; 
flow_blood.location_coords = [x; y]; flow_blood = rmfield(flow_blood,'beta');

% Parameter sweep 
parfor i = 1:numel(Gz)
    prot = set_dantespace_parameters('OV'); 
    prot.Gz = Gz(i);
    
    [~,~,~,~,~,blood_sig_Gz(i)] = epg_dantespace(prot, T1_blood, T2_blood, 0, flow_blood); %1 TR
    
    prot.n_TRs = 2;
    [~,~,~,~,~,VW_sig_Gz(i)] = epg_dantespace(prot, T1_VW, T2_VW, 0, flow_VW); %2 TRs
    [~,~,~,~,~,CSF_sig_Gz(i)] = epg_dantespace(prot, T1_CSF, T2_CSF, 0, flow_CSF); %2 TRs
end

%% Plot 4.1a/b/c
figure('Position',[100 100 1600 800])

% DANTE FAs
subplot(2,3,1), hold on, box on
set(gca,'FontSize',15)
plot(FAs,VW_sig_FA,'o-','LineWidth',2)
plot(FAs,CSF_sig_FA,'o-','LineWidth',2)
plot(FAs,blood_sig_FA,'o-','LineWidth',2)
plot([10 10], [0 0.15], 'k--')
legend('VW','CSF','Blood','Default','location','NorthEast')
ylabel('Signal / M_0')
xlim([-inf inf]), ylim([0 0.15])
title('a) Different DANTE FAs')

subplot(2,3,4), hold on, box on
set(gca,'FontSize',15)
plot(FAs,VW_sig_FA - CSF_sig_FA / 0.72,'o-','LineWidth',2,'color',matlabcolor(2))
plot(FAs,VW_sig_FA - blood_sig_FA / 0.72,'o-','LineWidth',2,'color',matlabcolor(3))
plot([10 10], [0 0.15], 'k--')
legend('VW/CSF','VW/Blood','Default','location','NorthEast')
xlabel('DANTE FA (�)'), ylabel('Contrast / VW M_0')
xlim([-inf inf]), ylim([0 0.15])

% Nr of DANTE pulses
subplot(2,3,2), hold on, box on
set(gca,'FontSize',15)
plot(Np_Ds,VW_sig_NpD,'o-','LineWidth',2)
plot(Np_Ds,CSF_sig_NpD,'o-','LineWidth',2)
plot(Np_Ds,blood_sig_NpD,'o-','LineWidth',2)
plot([300 300], [0 0.15], 'k--')
ylabel('Signal / M_0')
xlim([-inf inf]), ylim([0 0.15])
title('b) Number of DANTE pulses')

subplot(2,3,5), hold on, box on
set(gca,'FontSize',15)
plot(Np_Ds,VW_sig_NpD - CSF_sig_NpD / 0.72,'o-','LineWidth',2,'color',matlabcolor(2))
plot(Np_Ds,VW_sig_NpD - blood_sig_NpD / 0.72,'o-','LineWidth',2,'color',matlabcolor(3))
plot([300 300], [0 0.15], 'k--')
xlabel('Number of DANTE pulses'), ylabel('Contrast / VW M_0')
xlim([-inf inf]), ylim([0 0.15])

% Gradient strength
subplot(2,3,3), hold on, box on
set(gca,'FontSize',15)
plot(Gz,VW_sig_Gz,'o-','LineWidth',2)
plot(Gz,CSF_sig_Gz,'o-','LineWidth',2)
plot(Gz,blood_sig_Gz,'o-','LineWidth',2)
plot([1 1]*30.5, [0 0.15], 'k--')
ylabel('Signal / M_0')
xlim([-inf inf]), ylim([0 0.15])
title('c) DANTE gradient strength')

subplot(2,3,6), hold on, box on
set(gca,'FontSize',15)
plot(Gz,VW_sig_Gz - CSF_sig_Gz / 0.72,'o-','LineWidth',2,'color',matlabcolor(2))
plot(Gz,VW_sig_Gz - blood_sig_Gz / 0.72,'o-','LineWidth',2,'color',matlabcolor(3))
plot([1 1]*30.5, [0 0.15], 'k--')
xlabel('G (mT/m)'), ylabel('Contrast / VW M_0')
xlim([-inf inf]), ylim([0 0.15])

% set(findall(gcf,'-property','FontSize'),'FontSize',19)

%% 4.2a) TR
disp('Starting 4.2a')
TR = 1.02:0.2:5.02;    %s

% Initialize output parameters
VW_sig_TR = zeros(size(TR));
CSF_sig_TR = zeros(size(TR));
blood_sig_TR = zeros(size(TR));

% Set tissue/flow parameters 
[flow_VW, T1_VW, T2_VW] = set_tissue_parameters('VW');
[flow_CSF, T1_CSF, T2_CSF] = set_tissue_parameters('CSF');
[flow_blood, T1_blood, T2_blood] = set_tissue_parameters('blood');
flow_blood.B1 = B1_CP_vessel; 
flow_blood.location_coords = [x; y]; flow_blood = rmfield(flow_blood,'beta');

% Parameter sweep 
parfor i = 1:numel(TR)
    prot = set_dantespace_parameters('OV'); 
    prot.TR = TR(i);
    
    [~,~,~,~,~,blood_sig_TR(i)] = epg_dantespace(prot, T1_blood, T2_blood, 0, flow_blood); %1 TR
    
    prot.n_TRs = 2;
    [~,~,~,~,~,VW_sig_TR(i)] = epg_dantespace(prot, T1_VW, T2_VW, 0, flow_VW); %2 TRs
    [~,~,~,~,~,CSF_sig_TR(i)] = epg_dantespace(prot, T1_CSF, T2_CSF, 0, flow_CSF); %2 TRs
end

%% 4.2b) DANTE interpulse time
disp('Starting 4.2b')
t_D = 0.6:0.1:2;    %ms

% Initialize output parameters
VW_sig_tD = zeros(size(t_D));
CSF_sig_tD = zeros(size(t_D));
blood_sig_tD = zeros(size(t_D));

% Set tissue/flow parameters 
[flow_VW, T1_VW, T2_VW] = set_tissue_parameters('VW');
[flow_CSF, T1_CSF, T2_CSF] = set_tissue_parameters('CSF');
[flow_blood, T1_blood, T2_blood] = set_tissue_parameters('blood');
flow_blood.B1 = B1_CP_vessel; 
flow_blood.location_coords = [x; y]; flow_blood = rmfield(flow_blood,'beta');

% Parameter sweep 
parfor i = 1:numel(t_D)
    prot = set_dantespace_parameters('OV'); 
    prot.t_D = t_D(i);
    
    [~,~,~,~,~,blood_sig_tD(i)] = epg_dantespace(prot, T1_blood, T2_blood, 0, flow_blood); %1 TR
    
    prot.n_TRs = 2;
    [~,~,~,~,~,VW_sig_tD(i)] = epg_dantespace(prot, T1_VW, T2_VW, 0, flow_VW); %2 TRs
    [~,~,~,~,~,CSF_sig_tD(i)] = epg_dantespace(prot, T1_CSF, T2_CSF, 0, flow_CSF); %2 TRs
end

%% 4.2c) SPACE interpulse time
disp('Starting 4.2c')
t_S = 3.0:0.2:6.4;    %ms

% Initialize output parameters
VW_sig_tS = zeros(size(t_S));
VW_FWHM_tS = zeros(size(t_S));
CSF_sig_tS = zeros(size(t_S));
blood_sig_tS = zeros(size(t_S));

% Set tissue/flow parameters
[flow_VW, T1_VW, T2_VW] = set_tissue_parameters('VW');
[flow_CSF, T1_CSF, T2_CSF] = set_tissue_parameters('CSF');
[flow_blood, T1_blood, T2_blood] = set_tissue_parameters('blood');
flow_blood.B1 = B1_CP_vessel; 
flow_blood.location_coords = [x; y]; flow_blood = rmfield(flow_blood,'beta');

% Parameter sweep 
parfor i = 1:numel(t_S)
    prot = set_dantespace_parameters('OV'); 
    prot.t_S = t_S(i);
    
    [~,~,~,~,~,blood_sig_tS(i)] = epg_dantespace(prot, T1_blood, T2_blood, 0, flow_blood); %1 TR
    
    prot.n_TRs = 2;
    [~,~,~,~,Mt_SPACE,VW_sig_tS(i)] = epg_dantespace(prot, T1_VW, T2_VW, 0, flow_VW); %2 TRs
    VW_FWHM_tS(i) = PSF_FWHM(Mt_SPACE,0);
    
    [~,~,~,~,~,CSF_sig_tS(i)] = epg_dantespace(prot, T1_CSF, T2_CSF, 0, flow_CSF); %2 TRs
end

%% Plot 4.2a/b/c
figure('Position',[100 100 1600 800])

% DANTE TR
subplot(2,3,1), hold on, box on
set(gca,'FontSize',15)
plot(TR,VW_sig_TR./sqrt(TR),'o-','LineWidth',2)
plot(TR,CSF_sig_TR./sqrt(TR),'o-','LineWidth',2)
plot(TR,blood_sig_TR./sqrt(TR),'o-','LineWidth',2)
plot([2.62 2.62], [0 0.066], 'k--')
legend('VW','CSF','Blood','Default','location','East')
ylabel('Signal / sqrt(TR)')
xlim([-inf inf]), ylim([0 0.065])
title('a) Repetition time')

subplot(2,3,4), hold on, box on
set(gca,'FontSize',15)
plot(TR,(VW_sig_TR - CSF_sig_TR / 0.72)./sqrt(TR),'o-','LineWidth',2,'color',matlabcolor(2))
plot(TR,(VW_sig_TR - blood_sig_TR / 0.72)./sqrt(TR),'o-','LineWidth',2,'color',matlabcolor(3))
plot([2.62 2.62], [0 0.066], 'k--')
legend('VW/CSF','VW/Blood','Default','location','SouthEast')
xlabel('TR (s)'), ylabel('Contrast / sqrt(TR)')
xlim([-inf inf]), ylim([0 0.065])

% DANTE interpulse time
subplot(2,3,2), hold on, box on
set(gca,'FontSize',15)
plot(t_D,VW_sig_tD,'o-','LineWidth',2)
plot(t_D,CSF_sig_tD,'o-','LineWidth',2)
plot(t_D,blood_sig_tD,'o-','LineWidth',2)
plot([1.6 1.6], [0 0.15], 'k--')
ylabel('Signal / M_0')
xlim([-inf inf]), ylim([0 0.15])
title('b) DANTE interpulse time')

subplot(2,3,5), hold on, box on
set(gca,'FontSize',15)
plot(t_D,VW_sig_tD - CSF_sig_tD / 0.72,'o-','LineWidth',2,'color',matlabcolor(2))
plot(t_D,VW_sig_tD - blood_sig_tD / 0.72,'o-','LineWidth',2,'color',matlabcolor(3))
plot([1.6 1.6], [0 0.15], 'k--')
xlabel('t_D (ms)'), ylabel('Contrast / VW M_0')
xlim([-inf inf]), ylim([0 0.15])

% SPACE echo spacing
subplot(2,3,3), hold on, box on
set(gca,'FontSize',15)
plot(t_S,VW_sig_tS,'o-','LineWidth',2)
plot(t_S,CSF_sig_tS,'o-','LineWidth',2)
plot(t_S,blood_sig_tS,'o-','LineWidth',2)
plot([4.64 4.64], [0 0.15], 'k--')
ylabel('Signal / M_0')
xlim([-inf inf]), ylim([0 0.15])
title('c) SPACE echo spacing')

subplot(2,3,6), hold on, box on
set(gca,'FontSize',15)
plot(t_S,VW_sig_tS - CSF_sig_tS / 0.72,'o-','LineWidth',2,'color',matlabcolor(2))
plot(t_S,VW_sig_tS - blood_sig_tS / 0.72,'o-','LineWidth',2,'color',matlabcolor(3))
plot([4.64 4.64], [0 0.15], 'k--')
xlabel('t_S (ms)'), ylabel('Contrast / VW M_0')
xlim([-inf inf]), ylim([0 0.15]) 

%% Total time
time_finish = datetime();
disp('Total duration = '+string(time_finish-time_start'))