function [M,Mz,Mz_end,timepoints] = bloch_dante(alpha, full_duration, td, pw, gw, v, Gz, beta, T1, T2, p_0s)
%[M,Mz,Mz_end,timepoints] = bloch_dante(alpha, full_duration, td, pw, gw, v, Gz, beta, T1, T2, p_0s)
%Runs Bloch sims for full DANTE-preparation module (ref. Li2012)
%   
%   Note: this code is only used for validation of EPG simulations
%
%   Output parameters:
%   M = M magnetization vector that starts with [0 0 1]
%   Mz = longitudinal magnetization after each iteration
%   Mz_end = final longitudinal magnetization
%   timepoints = timing of Mz-points (s)
%   
%   Input parameters:
%   alpha = flip angle (degrees)
%   full_duration = duration of entire DANTE-module (ms)
%   td = duration of each DANTE-building block (ms)
%   pw = pulse duration/width (ms)
%   gw = gap duration/width (ms)
%   v = velocity of moving spins (cm/s)
%   Gz = gradient amplitude in the z-direction (mT/m)
%   beta = angle between v and k (degrees)
%   T1, T2 = relaxation times (ms)
%   p_0s (optional) = initial positions (cm)
%
% matthijs.debuck@ndcn.ox.ac.uk

if ~exist('p_0s','var')
    p_0s = 0;
end

% Prepare variables
alpha = alpha * pi / 180;       % degrees to radians
td = td/1000; pw = pw/1000; gw = gw/1000; T1 = T1/1000; T2 = T2/1000; full_duration = full_duration/1000; % ms to s
Gz = Gz / 1000;                 % mT/m to T/m
v = v / 100;                    % cm/s to m/s
b1_RF = alpha / (42.57747892e6*2*pi * pw); %required B1 to acquire the FA specified by alpha within pw
Np = double(int32(full_duration/td));   % Number of DANTE-pulses

timepoints = ((1:Np+1) - 1)/Np * full_duration;

el_block = 4;   % number of elements (discrete steps) per DANTE-block
b1 = zeros(1,Np*el_block); dT = zeros(1,Np*el_block); 
G = zeros(3,Np*el_block); p = zeros(3,numel(p_0s),Np*el_block);

for iteration = 1:Np
    %RF-pulse
    b1(iteration*el_block - el_block + 1) = b1_RF * double(1 - 2*mod(iteration+1,2));
    dT(iteration*el_block - el_block + 1) = pw;
    
    %gap
    dT(iteration*el_block - el_block + 2) = gw;

    %Gradient
    dT(iteration*el_block - el_block + 3) = td - pw - 2*gw;
    G(3,iteration*el_block - el_block + 3) = Gz;

    %gap
    dT(iteration*el_block) = gw;
end

p_change(1,1,:) = cumsum(dT) * v * sin(deg2rad(beta));
p_change(3,1,:) = cumsum(dT) * v * cos(deg2rad(beta));
for p_index = 1:numel(p_0s)
    p(3,p_index,:) = p(3,p_index,:) + p_0s(p_index)/100;
    p(:,p_index,:) = p(:,p_index,:) + p_change;
end

M = BlochSim( b1, dT, G, p, 0, T1, T2);
Mz = squeeze(M(3,1:4:end,:));
Mz_end = Mz(end);
end

