% Simulate the DANTE-attenuation for an ensemble of isochromats spaced over
% the distance of a full (2 pi) phase accrual along the gradient direction.
% In theory, these results should converge to the epg-DANTE results. 

%% DANTE-simulations - set initial parameters
% DANTE-parameters (based on Li et al: DOI 10.1002/mrm.24142)
td = 2;                       % DANTE interval (ms)
alpha = 7;                    % degrees
pw = 0.15;                    % DANTE pulse width (ms)
gw = 0.02;                    % DANTE gap width (ms) / gradient off time
Gz = 6;                       % mT/m
duration = td * 1000;         % ms

% Tissue properties:
T1 = 1500; T2 = 128;
beta = 0;

lambda = 1/(42.5 * Gz * (td - pw - gw*2)) * 100;    %wavelength of full phase-cycle (in cm)
velocities = [0.,0.1,0.5,1.0,5.0,10];

figure('Position',[100 200 1600 500])

%% Reconstruct Li2012 fig. 3a - single isochromat
%Simulate velocity-dependent attenuation profiles
clear Mz_diffv_iso
[~,Mz_diffv_iso(1,:),~,timepoints_bloch] = bloch_dante(alpha, duration, td, pw, gw, velocities(1), Gz, beta, T1, T2, 0);

Mz_diffv_iso(numel(velocities),:) = 0;
tic
parfor v_index = 1:numel(velocities)
    v = velocities(v_index);
    [~,Mz_diffv_iso(v_index,:),~,~] = bloch_dante(alpha, duration, td, pw, gw, v, Gz, beta, T1, T2, 0);
end
t_sim = toc;

% Plot results
subplot(1,3,1), hold on, box on
plot(timepoints_bloch, Mz_diffv_iso)
% legend('v = '+string(velocities)+' cm/s','Location','best')
ylim([-.5,1.]), yline(0,'--','HandleVisibility','off');
xlabel('Time (s)'), ylabel('M_z')
set(gca, 'FontSize',15)
% title('Li2012 - fig 3a')
title('a) Bloch isochromat; T_{sim} = '+string(round(t_sim,1))+' s')

%% Reconstruct Li2012 fig. 3a - with ensemble of isochromats
n_p0s = 1000;    %number of isochromats; higher = more accurate
p_0s = (0:n_p0s)/n_p0s+random('unif',zeros(1,n_p0s+1),1/n_p0s); %equally spaced with some random fluctuation (to prevent periodicity)

%Simulate velocity-dependent attenuation profiles
clear Mz_diffv_ens
[~,Mz_diffv_ens(1,:,:),~,timepoints_bloch] = bloch_dante(alpha, duration, td, pw, gw, velocities(1), Gz, beta, T1, T2, p_0s*lambda);
Mz_diffv_ens(numel(velocities),:,:) = 0;

tic
parfor v_index = 1:numel(velocities)
    v = velocities(v_index);
    [~,Mz_diffv_ens(v_index,:,:),~,~] = bloch_dante(alpha, duration, td, pw, gw, v, Gz, beta, T1, T2, p_0s*lambda);
end
t_sim = toc;

% Plot results
subplot(1,3,2), hold on, box on
plot(timepoints_bloch, mean(Mz_diffv_ens,3))
legend('v = '+string(velocities)+' cm/s','Location','best','FontSize',17)
ylim([-.5,1.]), yline(0,'--','HandleVisibility','off');
xlabel('Time (s)'), ylabel('M_z')
set(gca, 'FontSize',15)
title('b) Bloch ensemble (N = '+string(n_p0s)+'); T_{sim} = '+string(round(t_sim,1))+' s')

%% Reconstruct Li2012 fig. 3a - with EPGs
%Prepare EPG parameters (token values for SPACE parameters)
prot.t_D = td; prot.FA_DANTE = alpha; prot.Gz = Gz; prot.Np_D = 1000;
prot.TR = 5; prot.FAs_SPACE = [90 90 90]; prot.t_S = 1; prot.n_TRs = 1;
flow.vs = 0; flow.t_total = 10;

clear Mz_diffv_EPG
[~, Mz_diffv_EPG, ~, timepoints_epg] = epg_dantespace(prot, T1, T2, 0, flow);

Mz_diffv_EPG(numel(velocities),:) = 0;
tic
parfor v_index = 1:numel(velocities)
    flow = []; flow.t_total = 10; flow.pulsatile_bool = 0; 
    flow.vs = velocities(v_index);
    [~, Mz_diffv_EPG(v_index,:)] = epg_dantespace(prot, T1, T2, 0, flow);
end
t_sim = toc;

% Plot results
subplot(1,3,3), hold on, box on
plot(timepoints_epg(1:end-4), Mz_diffv_EPG(:,1:end-4))
% legend('v = '+string(velocities)+' cm/s','Location','best')
ylim([-.5,1.]), yline(0,'--','HandleVisibility','off');
xlabel('Time (s)'), ylabel('M_z')
set(gca, 'FontSize',15)
title('c) EPG; T_{sim} = '+string(round(t_sim,1))+' s')
