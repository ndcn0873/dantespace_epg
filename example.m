%% Example 1: Simple simulations (no B1, flow direction, etc)
% Compare vessel wall (VW) and blood signal evolution

% Set protocol parameters
prot = set_dantespace_parameters('TdB'); 

% Set parameters for (stationary) Vessel Wall tissue
[flow, T1, T2] = set_tissue_parameters('VW'); 

% Run simulation + plot transverse magnetization evolution
figure('Position', [200 200 1500 500])
plotter = true; opts.no_newfigure = true;

subplot(1,3,1)
prot.n_TRs = 1;     %1 TRs for VW does not achieve steady state
epg_dantespace(prot,T1,T2,plotter,flow,opts);
title('Vessel Wall - 1 TR')

subplot(1,3,2), opts.no_legend = true;
prot.n_TRs = 2;     %2 TRs for VW approximates the steady state
[~,~,~,~,Mt_SPACE_VW] = epg_dantespace(prot,T1,T2,plotter,flow,opts);
title('Vessel Wall - 2 TRs')

% Run simulation + plot transverse magnetization curve for blood
subplot(1,3,3), opts.no_legend = true;
prot.n_TRs = 1;     %1 TR for blood since it doesn't remain in RF field
[flow, T1, T2] = set_tissue_parameters('blood', 10); 
[~,~,~,~,Mt_SPACE_blood] = epg_dantespace(prot,T1,T2,plotter,flow,opts);
title('Blood')

% Compare results
[~, ampl_VW] = PSF_FWHM(Mt_SPACE_VW,0);
[~, ampl_blood] = PSF_FWHM(Mt_SPACE_blood,0);
ampl_VW = ampl_VW * 0.72;   %Proton density correction (see Viessmann 2017)

signal_ratio = ampl_VW / ampl_blood;
disp('      Result: Signal ratio (VW / blood) = '+string(signal_ratio))

%% Example 2: Simulation using B1 variation and an example flow trajectory
% Compare effect of ideal and realistic (7T CP) B1 on blood signal, in the
% case of substantial VW motion (i.e. a worse case comparison: reduced VW
% signal due to pulsatile VW motion + increased blood signal due to reduced
% DANTE efficiency)

load('HelperFiles/example_data.mat')

% Show B1 data + trajectory
figure('Position', [100 200 2000 500]), hold on
subplot(1,4,1)
imagesc(B1_CP_slice), axis square off, colormap gray, hold on
plot(x,y,'r', 'LineWidth',3)
caxis([0 1]), colorbar
title('B1 map + example vessel trajectory')

% Run simulation for VW (not plotted)
[flow, T1, T2] = set_tissue_parameters('VWmoving'); 
flow.vs = flow.vs / mean(flow.vs) * 0.1; %add substantial pulsatile VW motion to approximate VW-CSF interfaces (to 0.1 mm/s)
prot = set_dantespace_parameters('OV'); 
prot.n_TRs = 2;
[~,~,~,~,Mt_SPACE_VW] = epg_dantespace(prot,T1,T2,0,flow);
[~, ampl_VW] = PSF_FWHM(Mt_SPACE_VW,0);

% Extract B1-profiles along "vessel"
B1_CP_vessel = improfile(B1_CP_slice,x,y);
B1_CP_vessel = smooth(B1_CP_vessel,3);

% Prepare blood simulation variables
[flow, T1, T2] = set_tissue_parameters('blood'); prot.n_TRs = 1;
flow.location_coords = [x; y]; flow = rmfield(flow,'beta');

% Run simulations - note: volume under Mxy curve during SPACE ~= signal
subplot(1,4,2), opts.no_newfigure = true; opts.no_legend = true;
flow.B1 = B1_CP_vessel; 
[~,~,~,~,Mt_SPACE_B1vessel] = epg_dantespace(prot,T1,T2,plotter,flow,opts); ylim([0 0.05])
title('Blood - CP B1+')
[~, ampl_B1vessel] = PSF_FWHM(Mt_SPACE_B1vessel,0); opts.no_legend = false;

subplot(1,4,3), opts.no_newfigure = true; opts.no_legend = false;
flow.B1 = 1;
[~,~,~,~,Mt_SPACE_B1ideal] = epg_dantespace(prot,T1,T2,plotter,flow,opts); ylim([0 0.05])
title('Blood - Nominal B1+')
[~, ampl_B1ideal] = PSF_FWHM(Mt_SPACE_B1ideal,0);

%Show results
subplot(1,4,4)
hold on, box on
bar(1, ampl_VW)
bar(2:3, [ampl_B1ideal, ampl_B1vessel], 'r')
set(gca,'FontSize',15)
xticks(1:3); xticklabels({'Vessel Wall'; 'Blood_{ideal}'; 'Blood_{CP}'})
xlim([0.3 3.7])
ylabel('Signal (M_0 units)')

%% Example 3: Simulate effect of vessel wall motion on VW signal
% (To assess why/how using a reduced DANTE preparation (200 x 9 degrees)
% achieves better robustness against vessel wall motion than the 'original'
% DANTE preparation (300 x 10 degrees))

% Run simulations for different VW velocities (takes a while)

sweep = 0.9:0.0151:1.1;
vs = 0:0.0015:0.18;

peak_ampls_orig = zeros(size(vs));
peak_ampls_opt = zeros(size(vs));
peak_ampls_noDante = zeros(size(vs));

parfor index = 1:numel(vs)  %if no GPU is available, use for instead of parfor
    disp(string(index/numel(vs)*100)+'%') %track progress (linear for for-loop, indicative only for parfor)
    
    % initialize flow and prot in the loop for parfor
    [flow, T1, T2] = set_tissue_parameters('VW');
    [prot] = set_dantespace_parameters('OV');
    
    flow.vs = vs(index).*sweep;
    prot.FA_DANTE = 10; prot.n_TRs = 2; prot.Np_D = 300;
    [~,~,~,~,SPACE_VW] = epg_dantespace(prot, T1, T2, 0, flow);
    [~, peak_ampls_orig(index)] = PSF_FWHM(SPACE_VW);
    
    prot.FA_DANTE = 9; prot.n_TRs = 2; prot.Np_D = 200;
    [~,~,~,~,SPACE_VW] = epg_dantespace(prot, T1, T2, 0, flow);
    [~, peak_ampls_opt(index)] = PSF_FWHM(SPACE_VW);
    
    prot.n_TRs = 2; prot.Np_D = 0;
    [~,~,~,~,SPACE_VW] = epg_dantespace(prot, T1, T2, 0, flow);
    [~, peak_ampls_noDante(index)] = PSF_FWHM(SPACE_VW);
end
%%
% Plot results, showing VW signal gain when using reduced DANTE prep
figure, hold on, box on
plot(vs,peak_ampls_orig./peak_ampls_noDante*100,'LineWidth',2)
plot(vs,peak_ampls_opt./peak_ampls_noDante*100,'LineWidth',2)
plot(vs,(peak_ampls_opt-peak_ampls_orig)./peak_ampls_noDante*100,'LineWidth',2)
xlabel('Mean velocity (cm/s)')
ylabel('VW signal (%)')
legend('Full DANTE','Reduced DANTE','Difference')
set(gca, 'FontSize', 20)
ylim([0 100]), xlim([0 inf])

