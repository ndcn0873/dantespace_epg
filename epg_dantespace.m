function [Q, Mz, Mt, timepoints, Mt_SPACE_final, peak_ampl] = epg_dantespace(protocol, T1, T2, plotter, flow, opts)
%[Q,Mz,Mt,timepoints,Mt_SPACE_final] = epg_dantespace(protocol, T1, T2, plotter, flow, opts)
% 
%Simulates the 7T DANTE-SPACE sequence (Viessmann, MRM 2017) using EPGs,
%including the effect of different physiological and flow properties.
%
% Matthijs de Buck (matthijs.debuck@ndcn.ox.ac.uk), 2020-2022
% FMRIB, University of Oxford
%
% Uses some functions by B. Hargreaves (MRSignalsSeqs):
% epg_grad.m, epg_grelax.m, epg_mgrad.m, epg_rf.m
%
%   Input Parameters:
% 
% protocol: Struct containing the sequence/protocol parameters. Contains the following fields:
%   protocol.TR        = repetition time (s) (required)
%   protocol.n_TRs     = number of simulated TRs (optional; default = 1)
%   protocol.Np_D      = number of DANTE pulses per TR (required)
%   protocol.FA_DANTE  = DANTE flip angle (degr) (required)
%   protocol.Gz        = DANTE gradient strength (mT/m) (required; typically ~40)
%                        (currently assumed to be in z-direction)
%   protocol.t_D       = DANTE sub-TR (RF-RF distance) (ms) (required)
%   protocol.FAs_SPACE = array of SPACE flip angles (degr) (required)
%   protocol.t_S       = SPACE sub-TR (RF-RF interval) (ms) (required)
%
% T1, T2: Tissue relaxation times (ms) (required)
%
% plotter: bool - display the time vs transverse magnetization curve yes/no
%
% flow: Struct containing the properties related to the flow of the
%              simulated spins, including possible pulsatility/B1-changes.
%              If flow is empty, stationary spins are assumed.
%   flow.vs              = spin flow velocity (cm/s). If an array is given,
%                          the simulation returns the average results
%                          (magnetization) of spins with the selected
%                          velocity-values. (optional; default = 0)
%   flow.beta            = angle between DANTE gradient and flow direction 
%                          (degr) (optional; default = 0). Constant value.
%   flow.location_coords = coordinates ([x; y]) of flow trajectory. This
%                          is translated into an array of beta's which
%                          overrules the value in flow.beta (optional).
%                          Only works when simulating a single TR (n_TR=1).
%                          Note: currently assumed as 2D instead of 3D)
%   flow.B1              = array containing the relative B1-values, evenly 
%                          spaced out over a single pulse train.
%                          (optional; default = 1 = constant target FA).
%                          Can be real- or complex-valued. (WIP)
%                          NB: if n_TR > 1, B1 can only be a scalar!
%                          If size(B1,2) = 2, the first shim will be used
%                          for DANTE and the second for SPACE.
%   flow.pulsatile_bool  = use constant or pulsatile flow velocity
%                          variation (pulsatile assumes 60 beats/sec).
%                          (optional; default = true)
%   flow.t_total         = total time needed for blood spins to traverse
%                          the trajectory (used to set the timing for the
%                          B1 and orientation variation). In seconds.
%                          (optional; default = 1)
%   flow.profile_type    = pulsatile flow profile selection ('blood'/'CSF')
%                           (default = 'blood')
%   flow.diffusion       = include (water-like) diffusion in simulations
%                          (bool; default = false)
%   flow.diff_coeff      = diffusion coefficient (m^2/s; default = 3e-9)
%   flow.beta_SPACE      = Gradient direction during SPACE (i.e. anterior->
%                          posterior) is different from during DANTE; for 
%                          example in the case of blood, which roughly 
%                          moves in the coronal plane (both in the ICAs 
%                          and in the CoW), this means that the angle 
%                          between the flow direction and the gradients 
%                          will be near-orthogonal. An approximate value 
%                          for it can be set here (generally slightly
%                          off-orthogonal to still account for SPACE 
%                          attenuation effects). (degrees; default = 75; 
%                          set to [] to use same betas as in DANTE)
%
% opts: Struct containing uncommon/test/debug parameters
%   opts.pulsatile_timingfactor = offset (s) at the start of the first 
%                           heartbeat. (optional; default = rand(1)) (WIP)
%   opts.B1_scatterplot   = add plot of B1 magnitude during all pulses (as
%                           a check while debugging; slow implementation.
%                           (optional; default = false)
%                           scatter colors: black = DANTE, green =
%                           SPACE-excitation, red = SPACE variable FAs
%   opts.no_newfigure     = don't open a new figure for output plot (i.e.
%                           add to active plot) (bool; default = false)
%   opts.no_legend        = don't add a legend to output plot
%                           (bool; default = false)
%
%
%     Output Parameters:
%   Q                     = EPG states (3xN vector of F+, F- and Z states)
%   Mz                    = longitudinal magnetization (relative to M0)
%   Mt                    = transverse magnetization (relative to M0)
%   timepoints            = timing corresponding to values in Mz/Mt (s)
%   Mt_SPACE_final        = part of Mt corresponding to the final SPACE
%                           readout (assuming earlier readouts correspond
%                           to pre-equilibrium dummies, this can be used
%                           for PSF calculation)
%   peak_ampl             = peak amplitude of PSF (--> signal level) (M0)

%% ----------- Check required protocol parameters ----------- 
if ~(isfield(protocol,'FA_DANTE') && ~isempty(protocol.FA_DANTE))
    error('prot.FA_DANTE is not set properly (should be in degrees, not radians!)')
elseif (protocol.FA_DANTE < 1)
    warning('Make sure that prot.FA_DANTE is set properly (should be in degrees, not radians!)')
elseif ~(isfield(protocol,'n_TRs') && ~isempty(protocol.n_TRs) && (protocol.n_TRs > 0))
    error('prot.n_TRs is not set properly (probably unavailable or not > 0)')
elseif ~(isfield(protocol,'TR') && ~isempty(protocol.TR) && (protocol.TR < 500))
    error('prot.TR is not set properly (should be in s, not ms!)')
elseif ~(isfield(protocol,'FAs_SPACE') && ~isempty(protocol.FAs_SPACE) && (pi <= mean(protocol.FAs_SPACE)))
    error('prot.FAs_SPACE is not set properly (should be in degrees, not radians!)')
elseif ~(isfield(protocol,'t_S') && ~isempty(protocol.t_S) && (protocol.t_S > 0.1))
    error('prot.t_S is not set properly (should be in ms, not s!)')
elseif ~(isfield(protocol,'Np_D') && ~isempty(protocol.Np_D))
    error('prot.Np_D is not set properly')
elseif ~(isfield(protocol,'t_D') && ~isempty(protocol.t_D) && (protocol.t_D > 0.1))
    error('prot.t_D is not set properly (should be in ms, not s!)')
elseif ~(isfield(protocol,'Gz') && ~isempty(protocol.Gz))
    error('prot.Gz is not set properly (should be in mT/m!)')
end

%% ----------- Check required flow parameters (directions, velocities, B1s, etc) ----------- 
if ~exist('flow','var'), flow = []; end
if (isfield(flow,'location_coords') && ~isempty(flow.location_coords) && isfield(flow,'beta') && ~isempty(flow.beta))
    warning('flow.location_coords and flow.beta both set - ignoring flow.beta')
end
if ~(isfield(flow,'location_coords') && ~isempty(flow.location_coords))
    if ~(isfield(flow,'beta') && ~isempty(flow.beta))
        flow.beta = 0; %default: motion parallel to gradients, most effective suppression
    end
    betas = flow.beta; %constant direction
else
    xi = flow.location_coords(1,:); yi = flow.location_coords(2,:);
    dyi = abs(yi(1:end-1) - yi(2:end)) + eps; dxi = abs(xi(1:end-1) - xi(2:end)) + eps;
    angles = atan(dxi./dyi)/pi*180; %relative to y^
    distance = floor(cumsum(sqrt(dyi.^2+dxi.^2))*100); %*100 for more samples / interpolation

    betas = [];
    for i = 1:numel(distance)
        betas(numel(betas)+1:distance(i)) = angles(i);
    end
end
if numel(betas) > 1 && protocol.n_TRs > 1
    error('Trajectories are applied to a single pulse train - implementation doesnt work with multiple TRs!')
end
if ~(isfield(flow,'vs') && ~isempty(flow.vs))
    vs = 0; %default: stationary (no velocity)
else
    vs = flow.vs;
end
if ~(isfield(flow,'B1') && ~isempty(flow.B1))
    flow.B1 = 1; n_shim = 1;
    B1_all = 1; %assume perfect shim
    staticB1 = true; doubleStaticB1 = false;
else
    staticB1 = false; doubleStaticB1 = false; %default
    flow.B1(isnan(flow.B1)) = 0;
    n_shim = size(flow.B1,2);
    
    if size(flow.B1,1) == 1
        if n_shim == 1
            B1_all = abs(flow.B1); staticB1 = true; doubleStaticB1 = false;
            n_B1s = numel(B1_all);
        else
            staticB1 = false; doubleStaticB1 = true;
            B1_DANTE = abs(flow.B1(1,1)); B1_DANTE_phase = angle(flow.B1(1,1));
            B1_SPACE = abs(flow.B1(1,2)); B1_SPACE_phase = angle(flow.B1(1,2));
            n_B1s = numel(B1_DANTE);
        end
    else
        if size(flow.B1,1) < 30 %interpolate to prevent overfitting
            B1s_init = flow.B1; flow.B1 = [];
            x = 1:size(B1s_init,1); x_interp = 1:0.01:size(B1s_init,1); 
            for i = 1:n_shim
                flow.B1(:,i) = interp1(x,B1s_init(:,i),x_interp)';
            end
        end
        if n_shim == 1  %check if we use 1 RF shim or 2 RF shims (for DANTE + SPACE)
            B1_all = flow.B1; n_B1s = numel(B1_all);
        elseif n_shim == 2
            B1_DANTE = flow.B1(:,1); n_B1s = numel(B1_DANTE);
            B1_SPACE = flow.B1(:,2);
        end        
    end
    if n_shim > 2 
        error('Cant use more than 2 B1-shims')
    end
end
if size(flow.B1,2) > 1 && size(flow.B1,1) > 1 && protocol.n_TRs > 1
    error('B1 profiles are calculated over a single pulse train - they dont work with multiple TRs!')
end
if ~(isfield(flow,'pulsatile_bool') && ~isempty(flow.pulsatile_bool))
    pulsatile_bool = 1; %default = on
else, pulsatile_bool = flow.pulsatile_bool;
end
if ~(isfield(flow,'t_total') && ~isempty(flow.t_total))
    t_total = 1; %default = 1 sec
else, t_total = flow.t_total;
end
if ~(isfield(flow,'profile') && ~isempty(flow.profile))
    profile_type = 'blood'; %default
else
    profile_type = flow.profile;
end
if ~(isequal(profile_type, 'blood') || isequal(profile_type, 'CSF'))
    error('Invalid flow profile type')
end
if ~(isfield(flow,'diffusion') && ~isempty(flow.diffusion))
    diffusion_bool = false; %default
else
    diffusion_bool = flow.diffusion;
end
if  ~(isfield(flow,'diff_coeff') && ~isempty(flow.diff_coeff))
    diff_coeff = 3.0e-9;                 %Diffusion coefficient (m^2/s), from https://dtrx.de/od/diff/ for 37 degr (and confirmed by Wenchuan and Amy :-) )
else
    diff_coeff = flow.diff_coeff;
    if diff_coeff > 1e-8
        warning('Selected diffusion coefficient (flow.diff_coeff) is very large - make sure that it is in m^2/s units! (so 3e-9 for free water)')
    end
end
if (isfield(flow,'beta_SPACE') && ~isempty(flow.beta_SPACE))
    beta_SPACE = flow.beta_SPACE;
elseif ~isfield(flow,'beta_SPACE')
    beta_SPACE = 75;    %default value
end
%if beta_SPACE == [] (explicitly empty), it uses the DANTE-flow trajectory instead

%% ----------- Check opts ----------- 
if ~exist('opts','var'), opts = []; end
if ~(isfield(opts,'pulsatile_timingfactor') && ~isempty(opts.pulsatile_timingfactor))
    pulsatile_timingfactor = [];
else, pulsatile_timingfactor = opts.pulsatile_timingfactor;
end
if ~(isfield(opts,'B1_scatterplot') && ~isempty(opts.B1_scatterplot))
    B1scat = false; %default
else, B1scat = opts.B1_scatterplot;
end
if ~(isfield(opts,'no_newfigure') && ~isempty(opts.no_newfigure))
    no_newfigure = false; %default
else, no_newfigure = opts.no_newfigure;
end
if ~(isfield(opts,'no_legend') && ~isempty(opts.no_legend))
    no_legend = false; %default
else, no_legend = opts.no_legend;
end

%% ----------- Input parameters (set or fix) ----------- 
%some fixed parameters:
t_grad_SPACE = (2.43+2*.61)/1e3; % SPACE-gradient duration (s)
t_grad_SPACE_90 = 1.5e-3;        % duration of first SPACE-gradient (s)
Ag_S = 23.3e3 * t_grad_SPACE;    % area under SPACE-gradients (mT*ms/m)
Ag_S_90 = 28.3e3*t_grad_SPACE_90;% area under SPACE-gradient after 90-degr pulse (mT*ms/m)
t_SPACE_90 = 2.27e-3;            % pulse-pulse duration after first 90-degr SPACE-pulse (s)
dt_steps = 1;                    % number of downtime-steps (50 when plotting for better visualization; otherwise 1 to speed it up)
if plotter, dt_steps = 50; end  
pw = 0.15e-3;                    % DANTE pulse width (s)
gw = 0.02e-3;                    % DANTE gap width (s) / gradient off time
t_after_DANTE = 3.08e-3;         % additional gap between DANTE and SPACE (from IDEA simulations

%set variable parameters and fix units:
FA_DANTE = protocol.FA_DANTE * pi / 180;    % degrees to radians
FAs_SPACE = protocol.FAs_SPACE * pi/180;    % degrees to radians
T1 = T1*1e-3; T2 = T2*1e-3;                 % ms to s
t_D = protocol.t_D*1e-3; t_S = protocol.t_S*1e-3; % ms to s
t_grad_DANTE = t_D - pw - 2*gw;             % duration of DANTE-gradients
Ag_D = protocol.Gz * t_grad_DANTE*1e3;      % area under DANTE-gradients (mT*ms/m)
Np_S = numel(FAs_SPACE);                    % number of SPACE-RFs
T_D = protocol.Np_D * t_D + t_after_DANTE;                  % total DANTE-duration (s)
Np_D = protocol.Np_D; TR = protocol.TR; n_TRs = protocol.n_TRs;

time_all_pulses = Np_D * t_D + Np_S * t_S + t_SPACE_90 + t_after_DANTE;
downtime = TR - time_all_pulses;
if downtime < 0, error('Downtime < 0 (probably because TR < T_seq)'), end
iter_per_TR = Np_D + Np_S + dt_steps;

if time_all_pulses > t_total
    error('flow.t_total is too low to play out all required pulses for a single TR')
end

%% ----------- Final preparations - estimate B1 and trajectory coefficients ----------- 
time_all_pulses = TR - downtime;
%We assume that the total (B1/beta)-trajectory is covered in a fixed time, 
%given by flow.t_total (defaults to 1 second if not set). For example,
%if a pulse train only takes 0.6 sec and t_total  1 sec, only the last
%60% of the (B1/beta)-trajectory is included in the simulations, such that 
%the spins are in the "MCA" at the end of the readout. 
%The variable traj_included (below) is the factor of
%the trajectory that is included during the pulse train.
traj_included = time_all_pulses / t_total;   %pulses end at end of trajectory
traj_included_noD = (time_all_pulses-Np_D*t_D) / t_total;    %this is used separately for fitting, to ensure that the SPACE-B1 curves are identical with/without 0V-DANTE pulses in simulations (i.e. there are nog minor changes in the fits)

if ~staticB1 && ~doubleStaticB1    %if there are multiple B1s, apply a fit to interpolate the values at all possible timepoints
    time_fit = (0:ceil(n_B1s*traj_included)-1) / (ceil(n_B1s*traj_included)-1) * (time_all_pulses*1.02);  %linearly spaced along single pulse train (+2% extra to prevent weird behaviour at edges)
    time_fit_noD = Np_D*t_D+(0:ceil(n_B1s*traj_included_noD)-1) / (ceil(n_B1s*traj_included_noD)-1) * ((time_all_pulses-Np_D*t_D)*1.02);  %linearly spaced along single pulse train (+2% extra to prevent weird behaviour at edges)
    if n_shim == 1
        B1_all = B1_all(ceil(end*(1-traj_included)+eps):end);
        %Final letter in variable names indicates *D*ANTE or *S*PACE
        [B1_coeffD, B1_SD, B1_muD] = polyfit(time_fit,abs(B1_all'),14);   %only fits absolute values
        [B1_coeffS, B1_SS, B1_muS] = polyfit(time_fit,abs(B1_all'),14);   %only fits absolute values
        % Un-comment to check polynomial fit of B1s: (for debugging)
%         figure, plot(time_fit,abs(B1_all),'LineWidth',2), hold, plot(time_fit, polyval(B1_coeffD,time_fit, B1_SD, B1_muD),'LineWidth',2),xlabel('(Neck \leftarrow)               Location along vessel               (\rightarrow Brain)'), xticks([]), ylabel('Relative B1'), set(gca, 'FontSize',13), legend('Measured','Fit','Location','NorthWest','FontSize',13)
    else
        B1_DANTE = B1_DANTE(ceil(end*(1-traj_included)+eps):end);
        B1_SPACE = B1_SPACE(ceil(end*(1-traj_included_noD)+eps):end);
        %Final letter in variable names indicates *D*ANTE or *S*PACE
        [B1_coeffD, B1_SD, B1_muD] = polyfit(time_fit,abs(B1_DANTE'),14);   %only fits absolute values
        [B1_coeffS, B1_SS, B1_muS] = polyfit(time_fit_noD,abs(B1_SPACE'),14);   %only fits absolute values
        % Un-comment to check polynomial fits of B1s: (for debugging)
%         figure, plot(time_fit,abs(B1_DANTE),'LineWidth',2), hold on, plot(time_fit, polyval(B1_coeffD,time_fit, B1_SD, B1_muD),'LineWidth',2),xlabel('(Neck \leftarrow)               Location along vessel               (\rightarrow Brain)'),  ylabel('Relative B1'), set(gca, 'FontSize',13), legend('Measured','Fit','Location','NorthWest','FontSize',13)
%         figure, plot(time_fit_noD,abs(B1_SPACE),'LineWidth',2), hold on, plot(time_fit_noD, polyval(B1_coeffS, time_fit_noD, B1_SS, B1_muS),'LineWidth',2),xlabel('(Neck \leftarrow)               Location along vessel               (\rightarrow Brain)'), ylabel('Relative B1'), set(gca, 'FontSize',13), legend('Measured','Fit','Location','NorthWest','FontSize',13)
    end    
end

%if there are multiple locations, smoothen a bit and calculate
%corresponding timepoints + only keep portion of trajectory that is
%traversed during the pulse train (traj_included)
%NOTE: previous version used a polynomial fit; now removed
if (numel(betas) > 1)
    betas = smooth(betas, numel(betas)/100);
    n_betas = numel(betas);
    time_fit = (0:ceil(n_betas*traj_included)-1) / (ceil(n_betas*traj_included)-1) * (time_all_pulses*1.02);  %linearly spaced along single pulse train (+2% extra to prevent weird behaviour at edges)
    betas = betas(ceil(end*(1-traj_included)+eps):end);
end

%array containing the timing of the data in Mt and Mz (with corresponding indices) - 'timepoints' (= output array)
Np_D = double(Np_D); timepoints = [];
timepoints_TR = [(1:Np_D) * t_D, T_D + (1:Np_S) * t_S - t_S/2, TR-downtime+(1:dt_steps)*downtime/dt_steps];
timepoints = zeros(1,numel(timepoints)*n_TRs);
for n_TR = 1:n_TRs
    timepoints(1,(numel(timepoints_TR)*(n_TR-1)+1) : (numel(timepoints_TR)*n_TR)) = [timepoints_TR + TR * (n_TR-1)]; %timing is the same for every TR
end

%% ----------- Finally, actually run the simulations! ----------- 
Mt_mean = zeros(1,iter_per_TR*n_TRs); Mz_mean = zeros(1,iter_per_TR*n_TRs);
if B1scat
    figure, hold on, box on, set(gca,'FontSize',15);% ylim([0 2]);
    xlabel('Time (s)'); ylabel('Relative B1 (abs values)'); yticks(0:0.5:2);
end

%if B1 is a scalar, don't fit it during the simulations
if staticB1 && n_shim == 1, B1 = B1_all;  B1_phase = angle(B1_all); end
%if betas is a scalar, don't fit it during the simulations
if numel(betas) == 1, beta = betas; end

rng('default')
%resample from (100x) more values to ensure more distributed samples
rand_pulsatile_timingfactors = sort(rand(1,numel(vs)*100));
rand_pulsatile_timingfactors = rand_pulsatile_timingfactors(1:100:end);

for v_index = 1:numel(vs)
    v = vs(v_index);
    
    %Set a randomly initialized pulsatile velocity profile if desired
    if pulsatile_bool
        if exist('pulsatile_timingfactor','var') && ~isempty(pulsatile_timingfactor)
            v_profile = pulsatile_profile(v,timepoints(end),0,pulsatile_timingfactor,profile_type);
        else
            v_profile = pulsatile_profile(v,timepoints(end),0,rand_pulsatile_timingfactors(v_index),profile_type);
        end
    else, v_profile = v;
    end
    clear Q Mt Mz
    Q = [0 0 1]';                   % Z0=1 (Equilibrium)
    for n_TR = 1:n_TRs
        offset = iter_per_TR * (n_TR-1); %offset of iteration due to previous TRs
        
        %prepare diffusion parameters for DANTE
        if diffusion_bool
            D = diff_coeff;                    %Diffusion coefficient (m^2/s), from https://dtrx.de/od/diff/ for 37 degr (and confirmed by Wenchuan :-) )
            kg = Ag_D * 2 * pi * 42.577; %kg = int(Gdt)*2*pi*gamma = k-space traversal due to gradient (rad/m) for diffusion
        else
            D = 0; kg = 0;
        end
        for iteration = 1:Np_D  %DANTE
            t = timepoints(iteration+offset); 
            if ~staticB1
                if doubleStaticB1
                    B1 = B1_DANTE; B1_phase = B1_DANTE_phase;
                else
                    B1 = polyval(B1_coeffD,t,B1_SD,B1_muD);
                    if n_shim == 1, B1_phase = angle(B1_all(ceil(min((t-n_TR*TR+TR)/timepoints(Np_D+Np_S),1) * numel(B1_all)))); end
                    if n_shim == 2, B1_phase = angle(B1_DANTE(ceil(min((t-n_TR*TR+TR)/timepoints(Np_D+Np_S),1) * numel(B1_DANTE)))); end
                end
            end

            if numel(betas) > 1
%                 beta = polyval(beta_coeff,t,beta_S,beta_mu);
                beta = betas(max(find(time_fit <= t)));
            end
            
            if (v_index == 1 && B1scat), scatter(t,abs(B1),'k'), hold on, end
            
            v = v_profile(ceil(t/timepoints(end)*numel(v_profile)));
            
            if B1 > 0, Q = epg_rf(Q,FA_DANTE*B1,pi * double(mod(iteration-1,2)) + B1_phase); end %alternating phase
            
            Q = epg_flow(Q,v,t_grad_DANTE*1e3,Ag_D,beta,2);
            
            Q = epg_grelax(Q,T1,T2,t_D,kg,D,0);
            Q = epg_grad(Q); Q = epg_grad(Q);   %Gz approx. equal to Gz_SPACE*2

            Mz(iteration+offset) = abs(Q(3,1));   %longitudinal magnetization
            Mt(iteration+offset) = abs(Q(1,1));   %transverse magnetization
        end
        
        t = timepoints(Np_D+1+offset);
        if ~staticB1
            if doubleStaticB1
                B1 = B1_SPACE; B1_phase = B1_SPACE_phase;
            else
                B1 = polyval(B1_coeffS,t,B1_SS,B1_muS);
                if n_shim == 1, B1_phase = angle(B1_all(ceil(min((t-n_TR*TR+TR)/timepoints(Np_D+Np_S),1) * numel(B1_all)))); end
                if n_shim == 2, B1_phase = angle(B1_SPACE(ceil(min((t-n_TR*TR+TR)/timepoints(Np_D+Np_S),1) * numel(B1_SPACE)))); end
            end
        end
        if (v_index == 1 && B1scat), scatter(t,B1,'g'), hold on, end
        
        % gradient direction is different for SPACE; implementation
        % here depends on choice/use of flow.beta_SPACE; see
        % description at top of code for explanation
        if exist('beta_SPACE','var')
            beta_pulse = beta_SPACE;
        elseif numel(betas) > 1
%                 beta = polyval(beta_coeff,t,beta_S,beta_mu);
                beta = betas(max(find(time_fit <= t)));
        else
            beta_pulse = betas;
        end
        
        v = v_profile(ceil(t/timepoints(end)*numel(v_profile)));
        
        %Crusher gradient + delay between DANTE and SPACE
        Q(1:2,:) = 0;
        Q = epg_grelax(Q,T1,T2,t_after_DANTE,0,0,0,0);
        
        Q = Q(:,1:find(sum(abs(Q),1),1,'last')); %Remove trailing zeros (to speed up computation)
        
        %Prepare diffusion parameters for SPACE excitation pulse
        if diffusion_bool
            D = diff_coeff;                    %Diffusion coefficient (m^2/s)
            kg = Ag_S_90 * 2 * pi * 42.577; %kg = int(Gdt)*2*pi*gamma = k-space traversal due to gradient (rad/m) for diffusion
        else
            D = 0; kg = 0;
        end
        
        %First SPACE-pulse (excitation)
        Q = epg_rf(Q,FAs_SPACE(1)*abs(B1),pi/2 + B1_phase);
        Q = epg_flow(Q,v,t_grad_SPACE_90*1e3,Ag_S_90,beta_pulse,1);
        Q = epg_grelax(Q,T1,T2,t_SPACE_90,kg,D,1,0); %Note: 2 gradients applied, which is used for all future gradients to enable mid-gradient ADC sampling

        Mz(Np_D+1+offset) = Q(3,1);   %longitudinal magnetization
        Mt(Np_D+1+offset) = Q(1,1);   %transverse magnetization
        
        %Prepare diffusion parameters for SPACE
        if diffusion_bool
            D = diff_coeff;                   %Diffusion coefficient (m^2/s)
            kg = Ag_S * 2 * pi * 42.577; %kg = int(Gdt)*2*pi*gamma = k-space traversal due to gradient (rad/m) for diffusion
        else
            D = 0; kg = 0;
        end        
        for iteration = 2:Np_S  %rest of SPACE
            t = timepoints(Np_D + offset + iteration);
            if ~staticB1
                if doubleStaticB1
                    B1 = B1_SPACE; B1_phase = B1_SPACE_phase;
                else
                    B1 = polyval(B1_coeffS,t,B1_SS,B1_muS);
                    if n_shim == 1, B1_phase = angle(B1_all(ceil(min((t-n_TR*TR+TR)/timepoints(Np_D+Np_S),1) * numel(B1_all)))); end
                    if n_shim == 2, B1_phase = angle(B1_SPACE(ceil(min((t-n_TR*TR+TR)/timepoints(Np_D+Np_S),1) * numel(B1_SPACE)))); end
                end
            end
            
            % gradient direction is different for SPACE; implementation
            % here depends on choice/use of flow.beta_SPACE; see
            % description at top of code for explanation
            if exist('beta_SPACE','var')
                beta_pulse = beta_SPACE;
            elseif numel(betas) > 1
%                 beta = polyval(beta_coeff,t,beta_S,beta_mu);
                beta = betas(max(find(time_fit <= t)));
            else
                beta_pulse = betas;
            end
            
            v = v_profile(ceil(t/timepoints(end)*numel(v_profile)));
            
            if (v_index == 1 && B1scat), scatter(t,abs(B1),'r'), hold on, end
            Q = epg_rf(Q,FAs_SPACE(iteration)*abs(B1),B1_phase);
            Q = epg_flow(Q,v,t_grad_SPACE*1e3,Ag_S,beta_pulse,2);
            
            Q = epg_grelax(Q,T1,T2,t_S/2,kg,D,0,0);
            Q = epg_grad(Q);            % first half of gradient

            %sample values halfway through (centre of t_S)
            Mz(Np_D+iteration+offset) = Q(3,1);   %longitudinal magnetization
            Mt(Np_D+iteration+offset) = Q(1,1);   %transverse magnetization

            Q = epg_grelax(Q,T1,T2,t_S/2,kg,D,0,0);
            if iteration ~= Np_S, Q = epg_grad(Q); end % second half of gradient
        end

        for dt_step = 1:dt_steps  %downtime_steps (>1 step for more realistic visualization)
            Q = epg_grelax(Q,T1,T2,downtime/dt_steps,0,0,0,0); %downtime
            Mz(Np_D+Np_S+dt_step+offset) = Q(3,1);   %longitudinal magnetization
            Mt(Np_D+Np_S+dt_step+offset) = Q(1,1);   %transverse magnetization
        end
    end
    Mt_mean = Mt_mean + Mt/numel(vs);
    Mz_mean = Mz_mean + Mz/numel(vs);
end

Mt = abs(Mt_mean); Mz = abs(Mz_mean);

%The transverse magnetization during the readouts of the final SPACE
%section (for contrast calculations/comparisons):
Mt_SPACE_final = Mt(end-dt_steps-Np_S+2:end-dt_steps);  %+2 in lower limit because 90 degr pulse doesn't have an ADC event

%Calculate signal amplitude (M0); note that we here use the full Mt data in
%chronological order, which should work since the k-space center crossing
%occurs nearly exactly in the middle of the pulse train. For different
%sampling reordering (eg T1-weighted), reorder Mt_SPACE_final accordingly.
[~, peak_ampl] = PSF_FWHM(Mt_SPACE_final,0);

%% ----------- Optional: Plot the transverse magnetization curve ----------- 
if ~exist('plotter','var'), plotter = false; end
if plotter
    grayNess = 0.;  %0 = black axes; 1 = white; in between = gray.
    
    if ~no_newfigure, figure, end
    hold on, box on
%     title('<v> = '+string(mean(vs))+' cm/s; T1/T2 = '+string(T1*1000)+'/'+string(T2*1000)+' ms')
    plot(-1, 2, 'k', 'lineWidth', 3)    % Workaround for first legend entry
    plot(-1, 2, 'b--', 'lineWidth', 2)    % Workaround for second legend entry
    
    for limit = [-.5 1]
        step_dt = (timepoints(2) - timepoints(1));
        for offset = 0:iter_per_TR:(n_TRs-1)*iter_per_TR
            if Np_D > 0, area([timepoints(offset+1)-step_dt/2,timepoints(Np_D+offset)+step_dt/2], [limit limit], 'FaceAlpha',0.2, 'EdgeAlpha',0.,'FaceColor','y'), end
            area([timepoints(max(Np_D,1)+offset)+step_dt*(1/2-(2*(Np_D==0))),timepoints(Np_D+Np_S+offset)+t_S/2], [limit limit], 'FaceAlpha',0.2, 'EdgeAlpha',0.,'FaceColor','g')
            area([timepoints(Np_D+Np_S+offset)+t_S/2,timepoints(iter_per_TR+offset)], [limit limit], 'FaceAlpha',0.2, 'EdgeAlpha',0.,'FaceColor','r')
        end
    end
    plot(timepoints, Mt, 'k', 'lineWidth', 3)   %plot magnetization evolution
    plot(timepoints, Mz, 'b--', 'lineWidth', 2)   %plot magnetization evolution
    if ~no_legend
        if Np_D > 0, legend('M_{XY}','|M_{Z}|','DANTE', 'SPACE', 'TR Delay','AutoUpdate','off','Location','northeast')
        else, legend('M_{XY}', '|M_{Z}|', 'SPACE', 'TR Delay','AutoUpdate','off','Location','northeast')
        end
    end
    set(gca, 'FontSize', 15, 'XColor', [1 1 1]*grayNess, 'YColor', [1 1 1]*grayNess)
    xlabel('Time (s)'), ylabel('Magnetization (M_0)')
    ylim([-0. 0.479]), xlim([0 timepoints(end-dt_steps+1)])
    
    % Calculate the relative SAR contributions from SPACE and DANTE, and
    % show the relative SAR compared to the parameters in Viessmann2017
    % (assuming constant input voltage and SPACE pulse durations; mainly
    % useful as an indication when changing main parameters in 1Tx)
    rel_SAR_DANTE = FA_DANTE.^2*Np_D/pw/TR;
    rel_SAR_SPACE = sum(FAs_SPACE(:).^2./[4e-4,ones(1,numel(FAs_SPACE)-1)*7e-4]')/TR;   %4e-4s = first pulse duration, 7e-4 = duration of other pulses
    relative_SAR = (rel_SAR_DANTE + rel_SAR_SPACE) / 8.2222e+04;   %8.2222e+04 is the value when using original Viessmann2017-parameters
    disp('Relative SAR = '+string(relative_SAR)+'; '+string(100*rel_SAR_DANTE/(rel_SAR_SPACE+rel_SAR_DANTE))+'% from DANTE')
    disp('PSF amplitude (M0 units) = '+string(peak_ampl));
end

end


